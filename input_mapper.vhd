-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  VD fpga
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  input_mapper.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  08/01/2020
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Simple block to allow a signal to get mapped to new signal or bus. 
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;


entity input_mapper is
    port (

		reg_o				: out std_logic;
		
		d_in_i			: in std_logic	
				  
		  
    );
end entity input_mapper;

architecture imp_input_mapper of input_mapper is


begin

-- logic


	reg_o <= d_in_i;

end architecture imp_input_mapper;

