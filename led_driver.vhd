-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  NG PVU
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  led_driver.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  24/10/2018 
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Test module to flash an LED at 2Hz
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;


entity led_driver is
    port 	(

        clock20mhz_i           :  in std_logic;
        reset_ni               :  in std_logic;
		  pulse_1ms_i				: in std_logic;						
			led_o  				: out std_logic
        );
end;

architecture RTL of led_driver is

	
	signal   counter_s 		  : std_logic_vector(7 downto 0) ;
	

	
begin

	-- map signals to ports
	led_o <= counter_s(7);



    --counter to flash the led
    led : 	process ( reset_ni, clock20mhz_i ) is
    begin
        if ( reset_ni = '0' ) then
            counter_s <= ( others => '0' );

        elsif Rising_Edge ( clock20mhz_i ) then
        
			if pulse_1ms_i = '1' then
				counter_s <= std_logic_vector(unsigned(counter_s) + 1);
			end if;

        end if;
    end process;






end architecture RTL;