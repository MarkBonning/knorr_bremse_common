-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  EX fpga
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  output4_enable.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  23/01/2019
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Monitors OP_ENABLE_FPGA and sets ISO4_OP_ENABLE appropriately 
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;


entity output4_enable is
    port 	(

      clock20mhz_i           		: in std_logic;
      reset_ni               		: in std_logic;
      output_enable_fpga_i			: in std_logic;
		output4_enable_o  			: out std_logic;
		pulse_1ms_i						: in std_logic
        );
end;

architecture RTL of output4_enable is

---- Constants -------------------------------------------------------------------------------

	constant output4_limit_s		  : integer := 1000;	-- 1000ms

----------------------------------------------------------------------------------------------

---- Signals ---------------------------------------------------------------------------------
	signal counter_output4_s 					:integer range 0 to output4_limit_s := 0;
	signal output4_enable_s 					: std_logic;
	signal output_enable_fpga_current_s		: std_logic;
----------------------------------------------------------------------------------------------
	
begin

	-- map signals to ports
	   output4_enable_o <= output4_enable_s;


    --counter to set the watchdog refresh signal
    output4 : 	process ( reset_ni, clock20mhz_i, output_enable_fpga_i ) is
    begin
        if ( reset_ni = '0' ) then
            counter_output4_s <= 0;
				output4_enable_s <= '0';
            output_enable_fpga_current_s <= output_enable_fpga_i;
        elsif Rising_Edge ( clock20mhz_i ) then

			-- if input zero, then zero the count else, increment the count
			if( output_enable_fpga_i = '0') then
				counter_output4_s <= 0;
				output4_enable_s <= '0';				
			elsif( counter_output4_s = output4_limit_s) then
				output4_enable_s <= '1';
			else
				output4_enable_s <= '0';				
				if pulse_1ms_i = '1' then
					counter_output4_s <= counter_output4_s + 1;
				end if;
			end if;

        end if;
    end process;



end architecture RTL;