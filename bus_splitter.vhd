-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  VD fpga
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  bus_splitter.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  23/08/2019
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Simple block to split a bus to individual signals. 
-- Helps with block diagram view
-- Allow Outputs to be piped from the mux while maintaining their signal name to
-- all other connections in the design
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;


entity bus_splitter is
    port (

		reg_i			: in std_logic_vector(15 downto 0);
		
		d15_o			: out std_logic;	
		d14_o			: out std_logic;	
		d13_o			: out std_logic;	
		d12_o			: out std_logic;	
		d11_o			: out std_logic;	
		d10_o			: out std_logic;	
		d09_o			: out std_logic;	
		d08_o			: out std_logic;	
		d07_o			: out std_logic;	
		d06_o			: out std_logic;	
		d05_o			: out std_logic;	
		d04_o			: out std_logic;	
		d03_o			: out std_logic;	
		d02_o			: out std_logic;	
		d01_o			: out std_logic;	
		d00_o			: out std_logic
				  
		  
    );
end entity bus_splitter;

architecture imp_bus_splitter of bus_splitter is

	

begin

-- logic

d15_o	<=	reg_i(15);
d14_o	<=	reg_i(14);
d13_o	<=	reg_i(13);
d12_o	<=	reg_i(12);
d11_o	<=	reg_i(11);
d10_o	<=	reg_i(10);
d09_o	<=	reg_i(9);
d08_o	<=	reg_i(8);
d07_o	<=	reg_i(7);
d06_o	<=	reg_i(6);
d05_o	<=	reg_i(5);
d04_o	<=	reg_i(4);
d03_o	<=	reg_i(3);
d02_o	<=	reg_i(2);
d01_o	<=	reg_i(1);
d00_o	<=	reg_i(0);


-------------------------------------------------------------------------------------------------------------------------------------------------------------------

end architecture imp_bus_splitter;

