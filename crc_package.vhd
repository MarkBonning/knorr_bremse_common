-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  PVU
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  crc_package.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  01/02/2019 
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Creates a 16 bit CRC from input bytes
-- crc(15:0) = 1+x^2+x^15+x^16;
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package crc_package is
  -- polynomial: x^16 + x^15 + x^2 + 1
  -- data width: 16
  -- convention: the first serial bit is D[15]
  	function nextCRC16_D16
		(Data: std_logic_vector(15 downto 0);
		 crc:  std_logic_vector(15 downto 0))
		return std_logic_vector;
	end crc_package;


package body crc_package is

  function nextCRC16_D16
    (Data: std_logic_vector(15 downto 0);
     crc:  std_logic_vector(15 downto 0))
    return std_logic_vector is

    variable d:      std_logic_vector(15 downto 0);
    variable c:      std_logic_vector(15 downto 0);
    variable newcrc: std_logic_vector(15 downto 0);

  begin
    d := Data;
    c := crc;

    newcrc(0) := d(12) xor d(11) xor d(8) xor d(4) xor d(0) xor c(0) xor c(4) xor c(8) xor c(11) xor c(12);
    newcrc(1) := d(13) xor d(12) xor d(9) xor d(5) xor d(1) xor c(1) xor c(5) xor c(9) xor c(12) xor c(13);
    newcrc(2) := d(14) xor d(13) xor d(10) xor d(6) xor d(2) xor c(2) xor c(6) xor c(10) xor c(13) xor c(14);
    newcrc(3) := d(15) xor d(14) xor d(11) xor d(7) xor d(3) xor c(3) xor c(7) xor c(11) xor c(14) xor c(15);
    newcrc(4) := d(15) xor d(12) xor d(8) xor d(4) xor c(4) xor c(8) xor c(12) xor c(15);
    newcrc(5) := d(13) xor d(12) xor d(11) xor d(9) xor d(8) xor d(5) xor d(4) xor d(0) xor c(0) xor c(4) xor c(5) xor c(8) xor c(9) xor c(11) xor c(12) xor c(13);
    newcrc(6) := d(14) xor d(13) xor d(12) xor d(10) xor d(9) xor d(6) xor d(5) xor d(1) xor c(1) xor c(5) xor c(6) xor c(9) xor c(10) xor c(12) xor c(13) xor c(14);
    newcrc(7) := d(15) xor d(14) xor d(13) xor d(11) xor d(10) xor d(7) xor d(6) xor d(2) xor c(2) xor c(6) xor c(7) xor c(10) xor c(11) xor c(13) xor c(14) xor c(15);
    newcrc(8) := d(15) xor d(14) xor d(12) xor d(11) xor d(8) xor d(7) xor d(3) xor c(3) xor c(7) xor c(8) xor c(11) xor c(12) xor c(14) xor c(15);
    newcrc(9) := d(15) xor d(13) xor d(12) xor d(9) xor d(8) xor d(4) xor c(4) xor c(8) xor c(9) xor c(12) xor c(13) xor c(15);
    newcrc(10) := d(14) xor d(13) xor d(10) xor d(9) xor d(5) xor c(5) xor c(9) xor c(10) xor c(13) xor c(14);
    newcrc(11) := d(15) xor d(14) xor d(11) xor d(10) xor d(6) xor c(6) xor c(10) xor c(11) xor c(14) xor c(15);
    newcrc(12) := d(15) xor d(8) xor d(7) xor d(4) xor d(0) xor c(0) xor c(4) xor c(7) xor c(8) xor c(15);
    newcrc(13) := d(9) xor d(8) xor d(5) xor d(1) xor c(1) xor c(5) xor c(8) xor c(9);
    newcrc(14) := d(10) xor d(9) xor d(6) xor d(2) xor c(2) xor c(6) xor c(9) xor c(10);
    newcrc(15) := d(11) xor d(10) xor d(7) xor d(3) xor c(3) xor c(7) xor c(10) xor c(11);
    return newcrc;
  end nextCRC16_D16;

end crc_package;
