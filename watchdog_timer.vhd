-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  EX fpga
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  watchdog_timer.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  16/01/2019
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Watchdog timer - maintains a pulse to the watchdog as long as the 
-- watchdog gating signals are correct
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;


entity watchdog_timer is
    port 	(

			clock20mhz_i           						: in std_logic;
			reset_ni               						: in std_logic;

			pll_lock_i										: in std_logic;						-- pll lock input, direct from pll
			axle_1_hold_valve_drive_running_i		: in std_logic;						-- flag that valve pwm is running
			axle_2_hold_valve_drive_running_i		: in std_logic;						-- flag that valve pwm is running

			axle_1_vent_valve_drive_running_i		: in std_logic;						-- flag that valve pwm is running
			axle_2_vent_valve_drive_running_i		: in std_logic;						-- flag that valve pwm is running
			
			axle_1_equalise_valve_drive_running_i	: in std_logic;						-- flag that valve pwm is running
			axle_2_equalise_valve_drive_running_i	: in std_logic;						-- flag that valve pwm is running

			link_valve_drive_running_i					: in std_logic;						-- flag that valve pwm is running
			
			sb_axle_1_hold_i								: in std_logic;						-- input state
			sb_axle_2_hold_i								: in std_logic;						-- input state
			
			wsp_axle_1_hold_i								: in std_logic;						-- input state
			wsp_axle_2_hold_i								: in std_logic;						-- input state

			sb_axle_1_vent_i								: in std_logic;						-- input state
			sb_axle_2_vent_i								: in std_logic;						-- input state
			
			wsp_axle_1_vent_i								: in std_logic;						-- input state
			wsp_axle_2_vent_i								: in std_logic;						-- input state
		
			axle_1_equalise_i								: in std_logic;						-- input state
			axle_2_equalise_i								: in std_logic;						-- input state
			
			link_i											: in std_logic;						-- input state
			
			
			watchdog_kick_o  								: out std_logic						-- signal to drive the watchdog kick pin
			
        );
end;

architecture RTL of watchdog_timer is

	signal pulse1ms_s							: std_logic;
	signal pulse1ms_counter_s				: unsigned(14 downto 0);
	constant pulse1ms_limit					: unsigned(14 downto 0) := "010011100010000";

	signal timeout_50ms_counter_s			: unsigned(5 downto 0);
	constant timeout_50ms_limit			: unsigned(5 downto 0) := "110010";
	

	signal counter_watchdog_s 		  		: unsigned(5 downto 0) ;
	constant watchdog_limit_s		  		: unsigned(5 downto 0) := "101000";	-- 80ms (40ms high, 40ms low)
	signal watchdog_kick_s 					: std_logic;

	signal pll_lock_timeout_s				: std_logic;									-- flag to indicate the pll lock has been lost for 50ms or more
	
	signal watchdog_inputs_error_s		: std_logic;	

	constant limit_100ms						: unsigned(6 downto 0) := "1100100";		-- 100ms count limit
	
	signal axle_1_hold_s						: std_logic;										-- flag to hold the required logic
	signal axle_1_hold_100ms_timeout_s	: std_logic;										-- flag the 100ms has timed out
	signal axle_1_hold_100ms_counter_s	: unsigned(6 downto 0);							-- current 100ms count
	
	signal axle_2_hold_s						: std_logic;										-- flag to hold the required logic
	signal axle_2_hold_100ms_timeout_s	: std_logic;										-- flag the 100ms has timed out
	signal axle_2_hold_100ms_counter_s	: unsigned(6 downto 0);							-- current 100ms count
	
	signal axle_1_vent_s						: std_logic;										-- flag to hold the required logic
	signal axle_1_vent_100ms_timeout_s	: std_logic;										-- flag the 100ms has timed out
	signal axle_1_vent_100ms_counter_s	: unsigned(6 downto 0);							-- current 100ms count
	
	signal axle_2_vent_s						: std_logic;										-- flag to hold the required logic
	signal axle_2_vent_100ms_timeout_s	: std_logic;										-- flag the 100ms has timed out
	signal axle_2_vent_100ms_counter_s	: unsigned(6 downto 0);							-- current 100ms count
	
	signal axle_1_equalise_s					: std_logic;										-- flag to hold the required logic
	signal axle_1_equalise_100ms_timeout_s	: std_logic;										-- flag the 100ms has timed out
	signal axle_1_equalise_100ms_counter_s	: unsigned(6 downto 0);							-- current 100ms count
	
	signal axle_2_equalise_s					: std_logic;										-- flag to hold the required logic
	signal axle_2_equalise_100ms_timeout_s	: std_logic;										-- flag the 100ms has timed out
	signal axle_2_equalise_100ms_counter_s	: unsigned(6 downto 0);							-- current 100ms count

	signal link_s									: std_logic;										-- flag to hold the required logic
	signal link_100ms_timeout_s				: std_logic;										-- flag the 100ms has timed out
	signal link_100ms_counter_s				: unsigned(6 downto 0);							-- current 100ms count
	
	
begin

	-- map signals to ports
   watchdog_kick_o <= watchdog_kick_s;

	--combinatorial logic
	
	-- any of the timeouts trips the watched error flag
	watchdog_inputs_error_s	<= 	pll_lock_timeout_s or 
											axle_1_hold_100ms_timeout_s or axle_2_hold_100ms_timeout_s or
											axle_1_vent_100ms_timeout_s or axle_2_vent_100ms_timeout_s or
											axle_1_equalise_100ms_timeout_s or axle_2_equalise_100ms_timeout_s or
											link_100ms_timeout_s;
	
	axle_1_hold_s 		<= axle_1_hold_valve_drive_running_i and not (sb_axle_1_hold_i and wsp_axle_1_hold_i);
	axle_2_hold_s 		<= axle_2_hold_valve_drive_running_i and not (sb_axle_2_hold_i and wsp_axle_2_hold_i);
	
	axle_1_vent_s 		<= axle_1_vent_valve_drive_running_i and not (sb_axle_1_vent_i and wsp_axle_1_vent_i);
	axle_2_vent_s 		<= axle_2_vent_valve_drive_running_i and not (sb_axle_2_vent_i and wsp_axle_2_vent_i);
	
	axle_1_equalise_s <= axle_1_equalise_valve_drive_running_i and not axle_1_equalise_i;
	axle_2_equalise_s <= axle_2_equalise_valve_drive_running_i and not axle_2_equalise_i;

	link_s 				<= link_valve_drive_running_i and not link_i;
	

-------------------------------------------------------------------------------------------------------------
--counter create a single 20mhz clock pulse at 1khz - make the timer more manageable
-------------------------------------------------------------------------------------------------------------
    clock_div : 	process ( reset_ni, clock20mhz_i) is
    begin
        if ( reset_ni = '0' ) then
            pulse1ms_counter_s <= ( others => '0' );
				pulse1ms_s <= '0';

			elsif Rising_Edge ( clock20mhz_i ) then

				-- if the limit is met, then pulse the 1ms flag
				if pulse1ms_counter_s = pulse1ms_limit then
					pulse1ms_counter_s <= ( others => '0' ) ;
					pulse1ms_s <= '1';
				else
					pulse1ms_s <= '0';
					pulse1ms_counter_s <= pulse1ms_counter_s + 1;
				end if;
        end if;
    end process;

-------------------------------------------------------------------------------------------------------------
-- timer to trip if pll lock has been lost for 50ms or more
-------------------------------------------------------------------------------------------------------------
    pll_lock_lost : 	process ( reset_ni, clock20mhz_i) is
    begin
        if ( reset_ni = '0' ) then
            timeout_50ms_counter_s <= ( others => '0' );
				pll_lock_timeout_s <= '0';
        elsif Rising_Edge ( clock20mhz_i ) then


				-- if timeout limit met, then set the pll lost flag
				if timeout_50ms_counter_s = timeout_50ms_limit then
					timeout_50ms_counter_s <= ( others => '0' ) ;
					pll_lock_timeout_s <= '1';
					
				-- if the pll has not lost lock then keep count at 0
				elsif pll_lock_i <= '1' then
					timeout_50ms_counter_s <= ( others => '0' ) ;
					pll_lock_timeout_s <= '0';
			
				-- else increase the timeout count as lock is lost
				else
					timeout_50ms_counter_s <= timeout_50ms_counter_s + 1;
					pll_lock_timeout_s <= '0';
				end if;
        end if;
    end process;

	 

-------------------------------------------------------------------------------------------------------------
-- timer to trip if axle 1 hold in error for 100ms or more
-------------------------------------------------------------------------------------------------------------
    axle_1_hold_error :	process ( reset_ni, clock20mhz_i) is
    begin
        if ( reset_ni = '0' ) then
            axle_1_hold_100ms_counter_s <= ( others => '0' );
				axle_1_hold_100ms_timeout_s <= '0';
        elsif Rising_Edge ( clock20mhz_i ) then


				-- if the logic is ok, keep count at 0
				if axle_1_hold_s <= '0' then
					axle_1_hold_100ms_counter_s <= ( others => '0' ) ;
					axle_1_hold_100ms_timeout_s <= '0';

					-- if timeout limit met, then set the timeout flag
				elsif axle_1_hold_100ms_counter_s >= limit_100ms then
					axle_1_hold_100ms_timeout_s <= '1';
					
			
				-- else increase the timeout count on the 1ms pulse
				elsif pulse1ms_s = '1' then

					axle_1_hold_100ms_counter_s <= axle_1_hold_100ms_counter_s + 1;
					axle_1_hold_100ms_timeout_s <= '0';
				end if;
        end if;
    end process;

-------------------------------------------------------------------------------------------------------------
-- timer to trip if axle 2 hold in error for 100ms or more
-------------------------------------------------------------------------------------------------------------
    axle_2_hold_error :	process ( reset_ni, clock20mhz_i) is
    begin
        if ( reset_ni = '0' ) then
            axle_2_hold_100ms_counter_s <= ( others => '0' );
				axle_2_hold_100ms_timeout_s <= '0';
        elsif Rising_Edge ( clock20mhz_i ) then


				-- if the logic is ok, keep count at 0
				if axle_2_hold_s <= '0' then
					axle_2_hold_100ms_counter_s <= ( others => '0' ) ;
					axle_2_hold_100ms_timeout_s <= '0';

					-- if timeout limit met, then set the timeout flag
				elsif axle_2_hold_100ms_counter_s >= limit_100ms then
					axle_2_hold_100ms_timeout_s <= '1';
					
			
				-- else increase the timeout count on the 1ms pulse
				elsif pulse1ms_s = '1' then
					axle_2_hold_100ms_counter_s <= axle_2_hold_100ms_counter_s + 1;
					axle_2_hold_100ms_timeout_s <= '0';
				end if;
        end if;
    end process;
	 
	 
-------------------------------------------------------------------------------------------------------------
-- timer to trip if axle 1 vent in error for 100ms or more
-------------------------------------------------------------------------------------------------------------
    axle_1_vent_error :	process ( reset_ni, clock20mhz_i) is
    begin
        if ( reset_ni = '0' ) then
            axle_1_vent_100ms_counter_s <= ( others => '0' );
				axle_1_vent_100ms_timeout_s <= '0';
        elsif Rising_Edge ( clock20mhz_i ) then


				-- if the logic is ok, keep count at 0
				if axle_1_vent_s <= '0' then
					axle_1_vent_100ms_counter_s <= ( others => '0' ) ;
					axle_1_vent_100ms_timeout_s <= '0';

					-- if timeout limit met, then set the timeout flag
				elsif axle_1_vent_100ms_counter_s >= limit_100ms then
					axle_1_vent_100ms_timeout_s <= '1';
					
			
				-- else increase the timeout count on the 1ms pulse
				elsif pulse1ms_s = '1' then
					axle_1_vent_100ms_counter_s <= axle_1_vent_100ms_counter_s + 1;
					axle_1_vent_100ms_timeout_s <= '0';
				end if;
        end if;
    end process;
	 
-------------------------------------------------------------------------------------------------------------
-- timer to trip if axle 2 vent in error for 100ms or more
-------------------------------------------------------------------------------------------------------------
    axle_2_vent_error :	process ( reset_ni, clock20mhz_i) is
    begin
        if ( reset_ni = '0' ) then
            axle_2_vent_100ms_counter_s <= ( others => '0' );
				axle_2_vent_100ms_timeout_s <= '0';
        elsif Rising_Edge ( clock20mhz_i ) then


				-- if the logic is ok, keep count at 0
				if axle_2_vent_s <= '0' then
					axle_2_vent_100ms_counter_s <= ( others => '0' ) ;
					axle_2_vent_100ms_timeout_s <= '0';

					-- if timeout limit met, then set the timeout flag
				elsif axle_2_vent_100ms_counter_s >= limit_100ms then
					axle_2_vent_100ms_timeout_s <= '1';
					
			
				-- else increase the timeout count on the 1ms pulse
				elsif pulse1ms_s = '1' then
					axle_2_vent_100ms_counter_s <= axle_2_vent_100ms_counter_s + 1;
					axle_2_vent_100ms_timeout_s <= '0';
				end if;
        end if;
    end process;

-------------------------------------------------------------------------------------------------------------
-- timer to trip if axle 1 equalise in error for 100ms or more
-------------------------------------------------------------------------------------------------------------
    axle_1_equalise_error :	process ( reset_ni, clock20mhz_i) is
    begin
        if ( reset_ni = '0' ) then
            axle_1_equalise_100ms_counter_s <= ( others => '0' );
				axle_1_equalise_100ms_timeout_s <= '0';
        elsif Rising_Edge ( clock20mhz_i ) then


				-- if the logic is ok, keep count at 0
				if axle_1_equalise_s <= '0' then
					axle_1_equalise_100ms_counter_s <= ( others => '0' ) ;
					axle_1_equalise_100ms_timeout_s <= '0';

					-- if timeout limit met, then set the timeout flag
				elsif axle_1_equalise_100ms_counter_s >= limit_100ms then
					axle_1_equalise_100ms_timeout_s <= '1';
					
			
				-- else increase the timeout count on the 1ms pulse
				elsif pulse1ms_s = '1' then
					axle_1_equalise_100ms_counter_s <= axle_1_equalise_100ms_counter_s + 1;
					axle_1_equalise_100ms_timeout_s <= '0';
				end if;
        end if;
    end process;
	 
-------------------------------------------------------------------------------------------------------------
-- timer to trip if axle 2 equalise in error for 100ms or more
-------------------------------------------------------------------------------------------------------------
    axle_2_equalise_error :	process ( reset_ni, clock20mhz_i) is
    begin
        if ( reset_ni = '0' ) then
            axle_2_equalise_100ms_counter_s <= ( others => '0' );
				axle_2_equalise_100ms_timeout_s <= '0';
        elsif Rising_Edge ( clock20mhz_i ) then


				-- if the logic is ok, keep count at 0
				if axle_2_equalise_s <= '0' then
					axle_2_equalise_100ms_counter_s <= ( others => '0' ) ;
					axle_2_equalise_100ms_timeout_s <= '0';

					-- if timeout limit met, then set the timeout flag
				elsif axle_2_equalise_100ms_counter_s >= limit_100ms then
					axle_2_equalise_100ms_timeout_s <= '1';
					
			
				-- else increase the timeout count on the 1ms pulse
				elsif pulse1ms_s = '1' then
					axle_2_equalise_100ms_counter_s <= axle_2_equalise_100ms_counter_s + 1;
					axle_2_equalise_100ms_timeout_s <= '0';
				end if;
        end if;
    end process;
	 

-------------------------------------------------------------------------------------------------------------
-- timer to trip if link in error for 100ms or more
-------------------------------------------------------------------------------------------------------------
    link_error :	process ( reset_ni, clock20mhz_i) is
    begin
        if ( reset_ni = '0' ) then
            link_100ms_counter_s <= ( others => '0' );
				link_100ms_timeout_s <= '0';
        elsif Rising_Edge ( clock20mhz_i ) then


				-- if the logic is ok, keep count at 0
				if link_s <= '0' then
					link_100ms_counter_s <= ( others => '0' ) ;
					link_100ms_timeout_s <= '0';

					-- if timeout limit met, then set the timeout flag
				elsif link_100ms_counter_s >= limit_100ms then
					link_100ms_timeout_s <= '1';
					
			
				-- else increase the timeout count on the 1ms pulse
				elsif pulse1ms_s = '1' then
					link_100ms_counter_s <= link_100ms_counter_s + 1;
					link_100ms_timeout_s <= '0';
				end if;
        end if;
    end process;
	 
-------------------------------------------------------------------------------------------------------------
-- watchdog counter
-------------------------------------------------------------------------------------------------------------
    watchdog : 	process ( reset_ni, clock20mhz_i ) is
    begin
        if ( reset_ni = '0' ) then
            counter_watchdog_s <= ( others => '0' );
				watchdog_kick_s <= '0';

			elsif Rising_Edge ( clock20mhz_i ) then
        
        		--reset the count at the end of a cycle
				if counter_watchdog_s = watchdog_limit_s then
					counter_watchdog_s <= ( others => '0' );

					-- invert the watchdog kick
					watchdog_kick_s <= not watchdog_kick_s;
						
				-- increase the count on the 1ms pulse if no error on the watchdog gating signals
				-- if these conditions are not met, then the count does not increase 
				-- the awatchdog will trip if the timeout period expires
				elsif pulse1ms_s = '1' and watchdog_inputs_error_s = '0'then
					counter_watchdog_s <= counter_watchdog_s + 1;
					
				end if;


        end if;
    end process;



end architecture RTL;