-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  VD fpga
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  input_register.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  08/01/2020
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Simple block to register an input. 
-- No reset used as data valid after 2 clock cyles from power on reset
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;


entity input_register is
    port (

		clock20m_i		: in std_logic;
	 
		reg_o				: out std_logic;
		
		d_in_i			: in std_logic	
				  
		  
    );
end entity input_register;

architecture imp_input_register of input_register is

signal reg_s : std_logic;	


begin

-- logic



-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- register the inputs
------------------------------------------------------------------------------------------------------------------------------------------------------------------


	-- no reset as data valid after just 2 clocks from Power On Reset

	process_input_register : process (clock20m_i)
	
	begin
			
		-- clock data in on rising edge of clock
		if rising_edge(clock20m_i) then	 
		
			reg_s <= d_in_i;
			reg_o <= reg_s;
		
		
		end if;
	end process;

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

end architecture imp_input_register;

