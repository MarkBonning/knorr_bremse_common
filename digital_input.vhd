-- -------------------------------------------------------------------------------
--                                                                          
--       Copyright  (c)  2018  ;  Knorr-Bremse Rail Systems (UK) Ltd        
--                                                                            
--        This program is protected by copyright and the information         
--         contained therein is confidential.  The program may not be         
--          copied and the information may not be used or disclosed           
--          except with the written permission of the proprietors            
--                        Knorr-Bremse Rail Systems (UK) Ltd.                 
--                                                                           
-- -------------------------------------------------------------------------------
--                                                                           
--             Project Name  :  EX FPGA
--                FPGA Name  :  Altera 10M08SAU169I7G                          
--                File Name  :  digital_input.vhd                                  
--                   Author  :  Mark Bonning 
--                     Date  :  13/05/2019 
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Generic Digital input filter
-- Timings as per EX spec and VD spec
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning - initial design
--             
-- -------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity digital_input is
	port 	(
	
		clock20mhz_i				: in std_logic;								-- main clock
		reset_ni						: in std_logic;		
		
		raw_input_i					: in std_logic;								-- external digital input signal

		
		filtered_output_o			: out std_logic;								-- filtered digital output (to register)
	
		pulse_enable_o				: out std_logic;
		
		enable_i						: in std_logic;								-- enables the filtered output and pulse enable outputs

		pulse_1ms					: in std_logic									-- single 20mhz clock pulse at 1khz (1ms)
		
		);
end; 

architecture RTL of digital_input is


---- Constants -------------------------------------------------------------------------------

	-- timer limits in milliseconds
	constant count_30ms_limit 	: integer  	:= 30;				-- 30ms 
	constant count_500ms_limit : integer  	:= 500;				-- 500ms
	constant count_62ms_limit 	: integer 	:= 62;				-- 62ms 

----------------------------------------------------------------------------------------------

---- Signals ---------------------------------------------------------------------------------

	signal filtered_output_s		: std_logic;
	signal pulse_enable_s			: std_logic;
	signal pulse_timer_s				: std_logic;
	
----------------------------------------------------------------------------------------------
	
begin


----------------------------------------------------------------------------------------------
-- Process to filter the digital input
----------------------------------------------------------------------------------------------
	proc_Filtered_Dig_InputX : process (reset_ni, clock20mhz_i, raw_input_i, pulse_1ms)

	variable count_30ms	      : integer range 0 to count_30ms_limit := 0;
	
	begin
		-- clear registers when reset is low
		if(reset_ni = '0') then
			count_30ms := 0;
			filtered_output_s <= raw_input_i;

		elsif rising_edge(clock20mhz_i) then	 

			-- enable_i must be high to allow the outputs to operate
			filtered_output_o <= filtered_output_s and enable_i;
		
			-- if input = output then clear the count
			if filtered_output_s = raw_input_i then 
				count_30ms := 0;
			else
				-- increment count on 1ms pulse
				if pulse_1ms = '1' then
					count_30ms := count_30ms + 1;
				end if;

				-- hit the time limit so update output
				if count_30ms = count_30ms_limit then
					filtered_output_s <= raw_input_i;
					count_30ms := 0;
				end if;
			end if;
			
	 	end if;
	end process;
	
----------------------------------------------------------------------------------------------
-- Process to create pulse timer
----------------------------------------------------------------------------------------------
	proc_pulse_timer : process (reset_ni, clock20mhz_i, pulse_1ms)

	variable count_500ms      : integer range 0 to count_500ms_limit := 0;
	
	begin
		-- clear registers when reset is low
		if(reset_ni = '0') then
			count_500ms := 0;
			pulse_timer_s	<= '0';

		elsif rising_edge(clock20mhz_i) then	 

			
			if pulse_timer_s = '1' and count_500ms < count_500ms_limit then
				
				-- increment the count on the 1ms pulse
				if pulse_1ms = '1' then
					count_500ms := count_500ms + 1;
				end if;
				
			elsif filtered_output_s = '0' and raw_input_i = '1' then
				count_500ms := 0;
				pulse_timer_s	<= '1';
				
			else
				pulse_timer_s	<= filtered_output_s;
			end if;

	 	end if;
	end process;	
	
----------------------------------------------------------------------------------------------
-- Process to control DIGITAL INPUT X PULSE ENABLE
----------------------------------------------------------------------------------------------
	proc_Digital_InputX_Pulse_Enable : process (reset_ni, clock20mhz_i, pulse_1ms)

	variable count_62ms	      : integer range 0 to count_62ms_limit := 0;
	
	begin
		-- reset conditions
		if(reset_ni = '0') then
			count_62ms := 0;
			pulse_enable_s <= '1';

		elsif rising_edge(clock20mhz_i) then	
	
			pulse_enable_o <= pulse_enable_s and enable_i;
	

			if pulse_timer_s = '0' then
				count_62ms := 0;
				pulse_enable_s <= '1';
			else
				-- if count limit hit, clear the enable
				if count_62ms = count_62ms_limit then

					pulse_enable_s <= '0';
				
				-- else increase the count on the 1ms pulse
				elsif pulse_1ms = '1' then

					count_62ms := count_62ms + 1;
					pulse_enable_s <= '1';
					
				end if;
			end if;
			
	 	end if;
	end process;
	
end architecture RTL;