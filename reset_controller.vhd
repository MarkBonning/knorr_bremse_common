-- -------------------------------------------------------------------------------
--                                                                          
--       Copyright  (c)  2018  ;  Knorr-Bremse Rail Systems (UK) Ltd        
--                                                                            
--        This program is protected by copyright and the information         
--         contained therein is confidential.  The program may not be         
--          copied and the information may not be used or disclosed           
--          except with the written permission of the proprietors            
--                        Knorr-Bremse Rail Systems (UK) Ltd.                 
--                                                                           
-- -------------------------------------------------------------------------------
--                                                                           
--             Project Name  :  
--                FPGA Name  :  Altera 10M08SAU169I7G                          
--                File Name  :  reset_controller.vhd                                  
--                   Author  :  Mark Bonning 
--                     Date  :  17/10/2019 
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Generates a reset pulse
-- Was using PLL, but that now used as part of watchdog controller
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning - initial design
--             
-- -------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use IEEE.std_logic_unsigned.all;

entity reset_control is
    Port ( 
           clk_i	 	: in  std_logic;                        -- main 20MHz clock in
           reset_no 	: out  std_logic
 			  );
end reset_control;

architecture Behavioral of reset_control is

signal reset_count : unsigned(27 downto 0);
signal reset_s : std_logic; 

begin

	reset_no <= reset_s;


----------------------------------------------------------
------                Clocking                  -------
----------------------------------------------------------
        
    reset_process : process (clk_i)
    begin
        
		  
		  if rising_edge(clk_i) then
            
            -- set reset_s high once counter IS 20,000,000 = 1 second for power supplies to settle
            if(reset_count > x"1312D00") then

					reset_count <= (others => '0');
					reset_s <= '1';
				
            elsif(reset_count < x"1312D00") then

					reset_s <= '0';

					 -- increment count on each clock cycle
					reset_count <= reset_count + 1 ;

            else
                reset_s <= '1';

					 
            end if;
            
        end if;
    end process;        

--	reset_no <= reset_s;



end Behavioral;
