-- -------------------------------------------------------------------------------
--                                                                          
--       Copyright  (c)  2018  ;  Knorr-Bremse Rail Systems (UK) Ltd        
--                                                                            
--        This program is protected by copyright and the information         
--         contained therein is confidential.  The program may not be         
--          copied and the information may not be used or disclosed           
--          except with the written permission of the proprietors            
--                        Knorr-Bremse Rail Systems (UK) Ltd.                 
--                                                                           
-- -------------------------------------------------------------------------------
--                                                                           
--             Project Name  :  EX FPGA
--                FPGA Name  :  Altera 10M08SAU169I7G                          
--                File Name  :  pwm_input.vhd                                  
--                   Author  :  Mark Bonning 
--                     Date  :  17/12/2018 
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Measures PWM high pulse width and period in clock cycles
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning - initial design
--             
-- -------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.std_logic_unsigned.all;


entity pwm_input is
	port 	(
	
		clock20m_i			: in std_logic;		
		reset_ni				: in std_logic;		
		
		PWMInput_i			: in std_logic;
		
		clock_divider_i	: in std_logic_vector(2 downto 0);
		
		periodCount_o		: out std_logic_vector(15 downto 0);
		highCount_o			: out std_logic_vector(15 downto 0)
		
		);
end; 

	
	
architecture RTL of pwm_input is


---- Constants -------------------------------------------------------------------------------

	constant timeout_limit_c		: unsigned(19 downto 0) := x"61a80";	-- 20ms limit
	constant period_limit_c			: integer := 65535;							-- max 16 bit count

----------------------------------------------------------------------------------------------

---- Signals ---------------------------------------------------------------------------------


	-- signals
	type states is ( START, SIGNAL_HIGH, SIGNAL_LOW, COPY, CLOCK_DELAY);
	signal state_s   	: states;

	signal pwm_clock_s	: std_logic;
	signal pwm_clock_count_s : std_logic_vector(3 downto 0);
	
	
	-- rising edge detect signals
	signal s0_s							: std_logic;										-- edge detect signal 0
	signal s1_s							: std_logic;										-- edge detect signal 1
	signal rising_detect_s			: std_logic;										-- single clock pulse on rising edge

	signal timeout_s					: std_logic;										-- flag to show the status of the timeout. 
	signal timeout_count_s			: unsigned(19 downto 0);						-- counter for the timeout
	
	
----------------------------------------------------------------------------------------------

	
begin
		
	--map registers to ports

-- single clock pulse on rising edge of PWMInput_i
rising_detect_s <= not s1_s and s0_s;


-- currently using fixed clock divider (20 / 8 = 2.5mhz)
pwm_clock_s <= pwm_clock_count_s(2);

-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Clock divider
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	proc_pwm_clock : process (reset_ni, clock20m_i)
	
	begin
		if rising_edge(clock20m_i) then	 

			-- increment count on 20mhz clock
			pwm_clock_count_s <= std_logic_vector(unsigned(pwm_clock_count_s + 1));
			

	 	end if;
	end process;


-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Rising edge logic
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	rising_detect : process (reset_ni, pwm_clock_s)
	begin
		if reset_ni = '0' then
			s0_s <= '0';
			s1_s <= '0';
		elsif rising_edge (pwm_clock_s) then
			s0_s <= PWMInput_i;
			s1_s <= s0_s;
		end if;
	end process;


-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Timeout logic
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	timeout : process (reset_ni, pwm_clock_s)
	begin
		if reset_ni = '0' then
			timeout_s <= '0';
			timeout_count_s <= (others => '0');
		elsif rising_edge (pwm_clock_s) then
		

			-- on the rising edge of of the pulse, clear count and timeout flag
			if rising_detect_s = '1' then
				timeout_s <= '0';
				timeout_count_s <= (others => '0');
				
			-- if we have hit the timeout limit, then set the flag
			elsif timeout_count_s = timeout_limit_c then
			
				timeout_s <= '1';
			
			-- else the count is incremented
			else
				timeout_count_s <= timeout_count_s + 1;
			end if;
		
		end if;
	end process;
	

-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- PWM read process
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	pwm_read : process (reset_ni, pwm_clock_s)

	variable period_s	      : integer range 0 to period_limit_c := 0;
	variable high_s	      : integer range 0 to period_limit_c := 0;
	
	begin
		-- clear registers when reset is low
		if(reset_ni = '0') then
			period_s := 0;
			high_s := 0;
			periodCount_o <= (others => '0');
			highCount_o <= (others => '0');
			state_s <= START;

		-- clock data in on rising edge of clock
		elsif rising_edge(pwm_clock_s) then	 

		
			-- if timeout has occured check the input state
			if timeout_s = '1' then
			
				-- if high, set the outputs to all ones
				if(PWMInput_i = '1') then
					periodCount_o <= (others => '1');
					highCount_o <= (others => '1');
				-- if low, set the outputs to all zeros
				else
					periodCount_o <= (others => '0');
					highCount_o <= (others => '0');
				end if;

			else
		
				-- state machine
				case state_s is

					when START =>		

						-- clear the counts
						period_s := 0;
						high_s := 0;

						-- wait for input to go high to start the counts
						if(PWMInput_i = '1') then
							state_s <= SIGNAL_HIGH;
						else
							state_s <= START;
						end if;
						
					-- signal is high, count highs and period
					when SIGNAL_HIGH =>		
					
						-- if less than the maximum count, then increment period and high counts
						if period_s < period_limit_c then
							period_s := period_s + 1;
							high_s := high_s + 1;
						end if;

						-- while high, stay in the same state
						if PWMInput_i = '1' then
							state_s <= SIGNAL_HIGH;
						-- when low move to the low count state
						else
							state_s <= SIGNAL_LOW;
						end if;

					-- signal is low, count period
					when SIGNAL_LOW =>		

						-- if period is less that the maximum count, then increment
						if period_s < period_limit_c then
							period_s := period_s + 1;
						end if;	

							-- stay here while low
						if(PWMInput_i = '0') then
							state_s <= SIGNAL_LOW;
						else

							state_s <= COPY;
						end if;
			

					when COPY =>	

							-- finished, so copy counts to outputs
							periodCount_o <= std_logic_vector(to_unsigned(period_s, periodCount_o'length));
							highCount_o <= std_logic_vector(to_unsigned(high_s, highCount_o'length));

							state_s <= CLOCK_DELAY;
					
					when CLOCK_DELAY =>

							state_s <= START;

					-- something went wrong, go back to reset
					when others =>
						state_s <= START;

				end case;
			
			end if;
	 	end if;
	end process;
	
	
		
	
end architecture RTL;