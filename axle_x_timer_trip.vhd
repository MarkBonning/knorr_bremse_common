-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  VD fpga
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  axle_x_timer_enable.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  25/04/2019
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Controls the WSP timer enable section of the Valve Drive FPGA
-- Used in VD fpga and LW fpga
--
-- In LW there is no health timer, so dive the health timer trip 
-- with zero to keep OR funtion happy
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;


entity axle_x_timer_enable is
    port (
    
		wsp_axle_x_hold_i							: in std_logic;			-- Command from micro
		emergency_i									: in std_logic;			-- Emergency input

		axle_x_timer_enable_o					: out std_logic;			-- Internal FPGA signal to Axle 1 Hold and Vent timer blocks.

		axle_x_health_timer_trip_i				: in std_logic;			-- Internal FPGA signal from health timer block
				  
		axle_x_hold_timer_trip_i				: in std_logic;			-- Internal FPGA signal from health timer block

		axle_x_vent_timer_trip_i				: in std_logic;			-- Internal FPGA signal from health timer block
		
		axle_x_timer_trip_o						: out std_logic			-- OR'd result of timer trip signals

				  
    );
end entity axle_x_timer_enable;

architecture imp_axle_x_timer_enable of axle_x_timer_enable is


---- Signals ---------------------------------------------------------------------------------

	signal	axle_x_wsp_active_s					: std_logic;			

	signal	emergency_s							: std_logic;
	
	signal	timer_trip_s						: std_logic;

----------------------------------------------------------------------------------------------
	
	
begin

-- timer logic
emergency_s <= emergency_i;							

-- timer enables set from any of 4 conditions
axle_x_timer_enable_o <= axle_x_wsp_active_s or emergency_s;

timer_trip_s <= axle_x_health_timer_trip_i or axle_x_hold_timer_trip_i or axle_x_vent_timer_trip_i;

axle_x_timer_trip_o <= timer_trip_s;

	-- 
	wsp_active : process (wsp_axle_x_hold_i, timer_trip_s)
	
	begin
		
		
		if wsp_axle_x_hold_i = '0' or timer_trip_s = '1' then

			axle_x_wsp_active_s <= '0';

		-- clock data in on rising edge of wsp_axle_x_hold_i
		elsif rising_edge(wsp_axle_x_hold_i) then	 

			if axle_x_vent_timer_trip_i = '0' then
				axle_x_wsp_active_s <= '1';
			end if;
		
		
		end if;
	end process;
	

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

end architecture imp_axle_x_timer_enable;

