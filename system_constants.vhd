-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  NG PVU
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  system_constants.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning
--                     Date  :  10/01/2020
--
-- -------------------------------------------------------------------------------
-- description
-- System constant
-- -------------------------------------------------------------------------------
-- revision 0.1 : Mark Bonning - Initial design
--
-- -------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
package system_constants is

constant ERROR 							: std_logic := '1';
constant NO_ERROR	 						: std_logic := '0';
constant HEALTHY 							: std_logic := '1';
constant UNHEALTHY 						: std_logic := '0';
constant FAULT 							: std_logic := '1';
constant NO_FAULT	 						: std_logic := '0';

	
end system_constants;






























































