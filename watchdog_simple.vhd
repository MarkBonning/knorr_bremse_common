-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  NG PVU
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  watchdog_simple.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  24/10/2018 
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Watchdog timer just keeps it running for now
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;


entity watchdog_simple is
    port 	(

			clock20mhz_i         :  in std_logic;
			pulse_1ms_i          :  in std_logic;
			reset_ni             :  in std_logic;
			watchdog_kick_o  		: out std_logic
      );
end;

architecture RTL of watchdog_simple is

	
---- Constants -------------------------------------------------------------------------------

	constant counter_valid_low_c        : unsigned(7 downto 0) := "00010100";   	-- 40ms

----------------------------------------------------------------------------------------------

---- Signals ---------------------------------------------------------------------------------
	
	signal   counter_s 		  	: unsigned(9 downto 0) ;
	signal watchdog_kick_s 		: std_logic;
----------------------------------------------------------------------------------------------

	
begin

	-- map signals to ports
   watchdog_kick_o <= watchdog_kick_s;

----------------------------------------------------------------------------------------------
-- Control counter to set the watchdog refresh signal
----------------------------------------------------------------------------------------------
    watchdog : 	process ( reset_ni, clock20mhz_i ) is
    begin
        if ( reset_ni = '0' ) then
            counter_s <= ( others => '0' );
				watchdog_kick_s <= '1';
				
			elsif rising_edge(clock20mhz_i) then

				if pulse_1ms_i = '1' then
        
					--reset the count at the end of a cycle
					if counter_s = counter_valid_low_c then
						counter_s <= ( others => '0' ) ;
						
						-- invert the watchdog kick
						watchdog_kick_s <= not watchdog_kick_s;
					else
						counter_s <= counter_s + 1;
					end if;
					
				end if;
				
        end if;
    end process;


end architecture RTL;