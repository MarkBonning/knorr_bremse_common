-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  VD fpga
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  bus_combine.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  23/08/2019
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Simple block to combine individual signal to a bus. 
-- Helps with block diagram view
-- Allow Inputs to be piped to the mux while maintaining their signal name to
-- all other connections in the design
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;


entity bus_combine is
    port (

		clock20m_i			: in std_logic;
		reset_ni		: in std_logic;
	 
		reg_o			: out std_logic_vector(15 downto 0);
		
		d15_i			: in std_logic;	
		d14_i			: in std_logic;	
		d13_i			: in std_logic;	
		d12_i			: in std_logic;	
		d11_i			: in std_logic;	
		d10_i			: in std_logic;	
		d09_i			: in std_logic;	
		d08_i			: in std_logic;	
		d07_i			: in std_logic;	
		d06_i			: in std_logic;	
		d05_i			: in std_logic;	
		d04_i			: in std_logic;	
		d03_i			: in std_logic;	
		d02_i			: in std_logic;	
		d01_i			: in std_logic;	
		d00_i			: in std_logic
				  
		  
    );
end entity bus_combine;

architecture imp_bus_combine of bus_combine is

signal reg_s : std_logic_vector(15 downto 0);	


begin

-- logic



-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- mux read 
-------------------------------------------------------------------------------------------------------------------------------------------------------------------



	-- register the inputs
	process_bus_combine : process (reset_ni, clock20m_i)
	
	begin
		-- clear registers when reset is low
		if(reset_ni = '0') then

			reg_s 	<= (others => '0');
			
			
		-- clock data in on rising edge of clock
		elsif rising_edge(clock20m_i) then	 
		
			reg_s <= d15_i & d14_i & d13_i & d12_i & d11_i & d10_i & d09_i & d08_i & d07_i & d06_i & d05_i & d04_i & d03_i & d02_i & d01_i & d00_i;
			
			reg_o <= reg_s;
		
		
		end if;
	end process;

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

end architecture imp_bus_combine;

