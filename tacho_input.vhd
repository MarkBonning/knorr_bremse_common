-- -------------------------------------------------------------------------------
--                                                                          
--       Copyright  (c)  2018  ;  Knorr-Bremse Rail Systems (UK) Ltd        
--                                                                            
--        This program is protected by copyright and the information         
--         contained therein is confidential.  The program may not be         
--          copied and the information may not be used or disclosed           
--          except with the written permission of the proprietors            
--                        Knorr-Bremse Rail Systems (UK) Ltd.                 
--                                                                           
-- -------------------------------------------------------------------------------
--                                                                           
--             Project Name  :  EX FPGA
--                FPGA Name  :  Altera 10M08SAU169I7G                          
--                File Name  :  pwm_input.vhd                                  
--                   Author  :  Mark Bonning 
--                     Date  :  14/05/2019
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- This module counts the number of 1MHz clock cycles between tacho pulses
-- The count uses 21 of 32bits. Should the count overflow into the 22nd bit, then a
-- minimum speed flag is set.
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning - initial design
--             
-- -------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.std_logic_unsigned.all;


entity tacho_input is
	port 	(
	
		clock20mhz_i			: in std_logic;		
		reset_ni			: in std_logic;		
		
		tacho_input_i		: in std_logic;									-- tacho pulse input
		tacho_count_o		: out std_logic_vector(31 downto 0);		-- current pulse count output

		min_speed_detected_o 	: out std_logic;								-- flag to indicate minimum speed
		max_speed_detected_o 	: out std_logic;								-- flag to indicate maximum speed
		
		freeze_i						:  in std_logic								-- use this signal to stop updates to the output. This is to allow the 
																							-- mux to do a 32 bit transfer without the data getting updated between reads
		
		);
end; 

architecture RTL of tacho_input is


---- Constants -------------------------------------------------------------------------------

constant max_speed_count_c		: unsigned(11 downto 0) := x"3b8";	-- count = 952 when tacho getting 21kHz pulses
constant overrun_bit_c			: integer := 25;							-- If Bit 25 set, this is the overrun bit to set min speed detect

----------------------------------------------------------------------------------------------

---- Signals ---------------------------------------------------------------------------------


-- signals
type states is ( PULSE, LOAD, CLEAR);
signal state_s   	: states;

-- local count. 
signal pulse_count_s : unsigned(31 downto 0);

-- rising edge detect signals
signal s0_s							: std_logic;								-- edge detect signal 0
signal s1_s							: std_logic;								-- edge detect signal 1
signal rising_detect_s			: std_logic;								-- single clock pulse on rising edge



begin

	
-- single clock pulse on rising edge of Tacho_Input_i
rising_detect_s <= not s1_s and s0_s;


----------------------------------------------------------------------------------------------
-- Rising edge logic
----------------------------------------------------------------------------------------------
	rising_detect : process (reset_ni, clock20mhz_i)
	begin
		if reset_ni = '0' then
			s0_s <= '0';
			s1_s <= '0';
		elsif rising_edge (clock20mhz_i) then
			s0_s <= Tacho_Input_i;
			s1_s <= s0_s;
		end if;
	end process;


----------------------------------------------------------------------------------------------
-- Tacho count logic
----------------------------------------------------------------------------------------------

	tacho_count : process (reset_ni, clock20mhz_i)
	
	begin
		-- clear registers when reset is low
		if(reset_ni = '0') then
			pulse_count_s 				<= (others => '0');
			tacho_count_o 				<= (others => '0');
			state_s 						<= PULSE;
			min_speed_detected_o 	<= '0';
			max_speed_detected_o 	<= '0';

		-- clock data in on rising edge of clock
		elsif rising_edge(clock20mhz_i) then	 
		
			-- if bit 25 gets set, then we have hit min speed
			min_speed_detected_o <= pulse_count_s(overrun_bit_c);
		
			case state_s is

				-- main state		
				when PULSE =>		
						

					-- on rising edge load counts.
					if rising_detect_s = '1' then

						-- edge found so clear the min speed detect
						pulse_count_s(overrun_bit_c) <= '0';
				
						-- move to load
						state_s <= LOAD;
						
					-- if top bit set, then overrun occured. 
					-- min speed flag is set above as bit 25
					elsif pulse_count_s(overrun_bit_c) = '1' then

						--clear the count but leave the min speed bit
						pulse_count_s(overrun_bit_c - 1 downto 0) <= (others => '0');

						-- move to load
						state_s <= LOAD;

					-- else increment the count
					else
						pulse_count_s <= pulse_count_s + 1;
						state_s <= PULSE;
					end if;


				when LOAD =>
				
					-- only update the registers if freeze is low
					if freeze_i = '0' then
						-- copy the used 25 bits of data to the output.
						tacho_count_o <= "0000000" & std_logic_vector(pulse_count_s(24 downto 0));
						
						-- if pulse count is less than the max speed count, then we have measured an invalid 
						-- speed top, so set max speed flag
						if pulse_count_s < max_speed_count_c then
							max_speed_detected_o <= '1';
						else
							max_speed_detected_o <= '0';
						end if;

					end if;
					
					state_s <= CLEAR;
					
				when CLEAR =>		
				
					-- clear the 25 used bits
					pulse_count_s(24 downto 0) <= (others => '0');
					state_s <= PULSE;

				-- something went wrong, go back to reset
				when others =>
					state_s <= PULSE;

			end case;
			
	 	end if;
	end process;
	
end architecture RTL;