-- -------------------------------------------------------------------------------
--                                                                          
--       Copyright  (c)  2018  ;  Knorr-Bremse Rail Systems (UK) Ltd        
--                                                                            
--        This program is protected by copyright and the information         
--         contained therein is confidential.  The program may not be         
--          copied and the information may not be used or disclosed           
--          except with the written permission of the proprietors            
--                        Knorr-Bremse Rail Systems (UK) Ltd.                 
--                                                                           
-- -------------------------------------------------------------------------------
--                                                                           
--             Project Name  :  
--                FPGA Name  :  Altera 10M08SAU169I7G                          
--                File Name  :  register_mux.vhd                                  
--                   Author  :  Mark Bonning 
--                     Date  :  16/08/2019 
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Register mux. Multiplexer for the spi interface to the registers
-- Generic mux to be used on all fpgas
--
-- Currently 28 independant read / write registers. 
-- To make a register read only do not use the associated write register. 
-- To make a register read / write externally join the required 
-- reg_xx_i port to the required reg_xx_o port.
-- 
-- Mux has no write lines on the inputs, data is expected to be latched by the
-- data provider.
-- 
-- Mux ignores data changes during a read cycle from the attached SPI module
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning - initial design
--             
-- -------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.crc_package.all;


entity register_mux is
	port 	(
	
		clock20m_i						: in std_logic;		
		reset_ni						: in std_logic;		

		spi_clk_i      				: in std_logic;  								-- spi clk from master
		spi_cs_ni        				: in std_logic;  								-- active low slave select
		spi_mosi_i         			: in std_logic;  								-- master out, slave in
		spi_miso_o         			: out std_logic;  								-- master in, slave out

		
		-- read regiters
		reg_00_i							: in std_logic_vector(15 downto 0); 	
		reg_01_i							: in std_logic_vector(15 downto 0); 	
		reg_02_i							: in std_logic_vector(15 downto 0); 	
		reg_03_i							: in std_logic_vector(15 downto 0); 	
		reg_04_i							: in std_logic_vector(15 downto 0); 	
		reg_05_i							: in std_logic_vector(15 downto 0); 	
		reg_06_i							: in std_logic_vector(15 downto 0); 	
		reg_07_i							: in std_logic_vector(15 downto 0); 	
		reg_08_i							: in std_logic_vector(15 downto 0); 	
		reg_09_i							: in std_logic_vector(15 downto 0); 	
		reg_10_i							: in std_logic_vector(15 downto 0); 	
		reg_11_i							: in std_logic_vector(15 downto 0); 	
		reg_12_i							: in std_logic_vector(15 downto 0); 	
		reg_13_i							: in std_logic_vector(15 downto 0); 	
		reg_14_i							: in std_logic_vector(15 downto 0); 	
		reg_15_i							: in std_logic_vector(15 downto 0); 	
		reg_16_i							: in std_logic_vector(15 downto 0); 	
		reg_17_i							: in std_logic_vector(15 downto 0); 	
		reg_18_i							: in std_logic_vector(15 downto 0); 	
		reg_19_i							: in std_logic_vector(15 downto 0); 	
		reg_20_i							: in std_logic_vector(15 downto 0); 	
		reg_21_i							: in std_logic_vector(15 downto 0); 	
		reg_22_i							: in std_logic_vector(15 downto 0); 	
		reg_23_i							: in std_logic_vector(15 downto 0); 	
		reg_24_i							: in std_logic_vector(15 downto 0); 	
		reg_25_i							: in std_logic_vector(15 downto 0); 	
		reg_26_i							: in std_logic_vector(15 downto 0); 	
		reg_27_i							: in std_logic_vector(15 downto 0); 	
		reg_28_i							: in std_logic_vector(15 downto 0); 	
		reg_29_i							: in std_logic_vector(15 downto 0); 	

		-- write regiters
		reg_00_o							: out std_logic_vector(15 downto 0); 	
		reg_01_o							: out std_logic_vector(15 downto 0); 	
		reg_02_o							: out std_logic_vector(15 downto 0); 	
		reg_03_o							: out std_logic_vector(15 downto 0); 	
		reg_04_o							: out std_logic_vector(15 downto 0); 	
		reg_05_o							: out std_logic_vector(15 downto 0); 	
		reg_06_o							: out std_logic_vector(15 downto 0); 	
		reg_07_o							: out std_logic_vector(15 downto 0); 	
		reg_08_o							: out std_logic_vector(15 downto 0); 	
		reg_09_o							: out std_logic_vector(15 downto 0); 	
		reg_10_o							: out std_logic_vector(15 downto 0); 	
		reg_11_o							: out std_logic_vector(15 downto 0); 	
		reg_12_o							: out std_logic_vector(15 downto 0); 	
		reg_13_o							: out std_logic_vector(15 downto 0); 	
		reg_14_o							: out std_logic_vector(15 downto 0); 	
		reg_15_o							: out std_logic_vector(15 downto 0); 	
		reg_16_o							: out std_logic_vector(15 downto 0); 	
		reg_17_o							: out std_logic_vector(15 downto 0); 	
		reg_18_o							: out std_logic_vector(15 downto 0); 	
		reg_19_o							: out std_logic_vector(15 downto 0); 	
		reg_20_o							: out std_logic_vector(15 downto 0); 	
		reg_21_o							: out std_logic_vector(15 downto 0); 	
		reg_22_o							: out std_logic_vector(15 downto 0); 	
		reg_23_o							: out std_logic_vector(15 downto 0); 	
		reg_24_o							: out std_logic_vector(15 downto 0);
		reg_25_o							: out std_logic_vector(15 downto 0);
		reg_26_o							: out std_logic_vector(15 downto 0);
		reg_27_o							: out std_logic_vector(15 downto 0);
		reg_28_o							: out std_logic_vector(15 downto 0);
		reg_29_o							: out std_logic_vector(15 downto 0);
		
		freeze_o							: out std_logic

		
		);
end; 

architecture RTL of register_mux is

	-- signals

	signal register_write_s : std_logic;
	signal register_read_s : std_logic;
	
	signal tx_data_s  				: std_logic_vector(0 to 47);  					-- spi transmit buffer (address copy, data and crc)
	signal rx_data_s					: std_logic_vector(0 to 48);					-- spi recieve buffer (including address bytes and crc)
	signal tx_bit_count_s 			: unsigned(7 downto 0);
	signal rx_bit_count_s 			: unsigned(7 downto 0);
	signal error_s						: std_logic;
	
	-- latched data in and out signals
	signal mux_data_out_s			: std_logic_vector(15 downto 0);
	signal mux_data_in_s				: std_logic_vector(15 downto 0);
	

	-- crc signals
	signal crc_calc_s : std_logic_vector(15 downto 0);
	signal crc_data_in_s : std_logic_vector(15 downto 0);
	signal crc_result_s : std_logic_vector(15 downto 0);
	
		-- rising edge detect signals
	signal s0_s							: std_logic;									-- edge detect signal 0
	signal s1_s							: std_logic;									-- edge detect signal 1
	signal rising_detect_s			: std_logic;									-- single clock pulse on rising edge
	
	 
begin
		
-- rising edge logic
rising_detect_s <= not S1_s and s0_s;

-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Rising / falling edge logic
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	rising_detect : process (reset_ni, clock20m_i)
	begin
		if reset_ni = '0' then
			s0_s <= '0';
			s1_s <= '0';
		elsif rising_edge (clock20m_i) then
			s0_s <= register_read_s;
			s1_s <= s0_s;
		end if;
	end process;	



-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- SPI TX
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	spi_tx : process (spi_cs_ni, spi_clk_i)
	
	
	begin
		if spi_cs_ni = '1' then

			tx_bit_count_s <= (others => '0');
			
			spi_miso_o <= '0';

		elsif falling_edge (spi_clk_i) then

			spi_miso_o <= tx_data_s(to_integer(tx_bit_count_s));  
			
			if tx_bit_count_s < 47 then
				tx_bit_count_s <= unsigned(tx_bit_count_s) + 1;
			end if;

		end if;
	end process;	


-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- SPI RX
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	spi_rx : process (spi_cs_ni, spi_clk_i)
	
	
	begin
	-- Use the chip select signal to clear all the counts and flags
		if spi_cs_ni = '1' then

			rx_bit_count_s 				<= (others => '0');
			rx_data_s 					<= (others => '0');
			

		-- clock the process o nthe ruising edge of the SPI clock
		elsif rising_edge (spi_clk_i) then

			-- load the rx register
			rx_data_s(to_integer(rx_bit_count_s)) <= spi_mosi_i;

			-- increment the bit count each lap as long as we dont over run the buffers.
			--if you send an SPI message with extra bits / bytes this will stop it from crashing
			if rx_bit_count_s < 47 then
				rx_bit_count_s <= unsigned(rx_bit_count_s) + 1;
			end if;
					
			
		end if;
	end process;
	
			

-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- mux read 
-------------------------------------------------------------------------------------------------------------------------------------------------------------------




	-- sets the spi output data depending on the register address
	mux_read : process (reset_ni, clock20m_i)
	
	begin
		-- clear registers when reset is low
		if(reset_ni = '0') then

			mux_data_out_s 	<= (others => '0');
			
		-- clock data in on rising edge of clock
		elsif rising_edge(clock20m_i) then	 

			if rising_detect_s = '1' then
		
				-- select the register to read
				case rx_data_s(2 to 6) is

					when "00000" => mux_data_out_s <= reg_00_i;
					when "00001" => mux_data_out_s <= reg_01_i;
					when "00010" => mux_data_out_s <= reg_02_i;
					when "00011" => mux_data_out_s <= reg_03_i;
					when "00100" => mux_data_out_s <= reg_04_i;
					when "00101" => mux_data_out_s <= reg_05_i;
					when "00110" => mux_data_out_s <= reg_06_i;
					when "00111" => mux_data_out_s <= reg_07_i;
					when "01000" => mux_data_out_s <= reg_08_i;
					when "01001" => mux_data_out_s <= reg_09_i;
					when "01010" => mux_data_out_s <= reg_10_i;
					when "01011" => mux_data_out_s <= reg_11_i;
					when "01100" => mux_data_out_s <= reg_12_i;
					when "01101" => mux_data_out_s <= reg_13_i;
					when "01110" => mux_data_out_s <= reg_14_i;
					when "01111" => mux_data_out_s <= reg_15_i;
					when "10000" => mux_data_out_s <= reg_16_i;
					when "10001" => mux_data_out_s <= reg_17_i;
					when "10010" => mux_data_out_s <= reg_18_i;
					when "10011" => mux_data_out_s <= reg_19_i;
					when "10100" => mux_data_out_s <= reg_20_i;
					when "10101" => mux_data_out_s <= reg_21_i;
					when "10110" => mux_data_out_s <= reg_22_i;
					when "10111" => mux_data_out_s <= reg_23_i;
					when "11000" => mux_data_out_s <= reg_24_i;
					when "11001" => mux_data_out_s <= reg_25_i;
					when "11010" => mux_data_out_s <= reg_26_i;
					when "11011" => mux_data_out_s <= reg_27_i;
					when "11100" => mux_data_out_s <= reg_28_i;
					when "11101" => mux_data_out_s <= reg_29_i;

					when others => null;
				end case;
			end if;

		end if;
	end process;
	
	
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- mux write 
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	-- receive data from spi module and send to registers
	mux_write : process (reset_ni, clock20m_i)
	
	begin
		-- clear registers when reset is low
		if(reset_ni = '0') then

			reg_00_o <= (others => '0');
			reg_01_o <= (others => '0');
			reg_02_o <= (others => '0');
			reg_03_o <= (others => '0');
			reg_04_o <= (others => '0');
			reg_05_o <= (others => '0');
			reg_06_o <= (others => '0');
			reg_07_o <= (others => '0');
			reg_08_o <= (others => '0');
			reg_09_o <= (others => '0');
			reg_10_o <= (others => '0');
			reg_11_o <= (others => '0');
			reg_12_o <= (others => '0');
			reg_13_o <= (others => '0');
			reg_14_o <= (others => '0');
			reg_15_o <= (others => '0');
			reg_16_o <= (others => '0');
			reg_17_o <= (others => '0');
			reg_18_o <= (others => '0');
			reg_19_o <= (others => '0');
			reg_20_o <= (others => '0');
			reg_21_o <= (others => '0');
			reg_22_o <= (others => '0');
			reg_23_o <= (others => '0');
			reg_24_o <= (others => '0');
			reg_25_o <= (others => '0');
			reg_26_o <= (others => '0');
			reg_27_o <= (others => '0');
			reg_28_o <= (others => '0');
			reg_29_o <= (others => '0');


		-- clock data in on rising edge of clock
		elsif rising_edge(clock20m_i) then	 
		

			-- when register write is high, load register
			if register_write_s = '1' then


				-- select the register to write
				case rx_data_s(2 to 6) is

					when "00000" => reg_00_o <= mux_data_in_s;
					when "00001" => reg_01_o <= mux_data_in_s;
					when "00010" => reg_02_o <= mux_data_in_s;
					when "00011" => reg_03_o <= mux_data_in_s;
					when "00100" => reg_04_o <= mux_data_in_s;
					when "00101" => reg_05_o <= mux_data_in_s;
					when "00110" => reg_06_o <= mux_data_in_s;
					when "00111" => reg_07_o <= mux_data_in_s;
					when "01000" => reg_08_o <= mux_data_in_s;
					when "01001" => reg_09_o <= mux_data_in_s;
					when "01010" => reg_10_o <= mux_data_in_s;
					when "01011" => reg_11_o <= mux_data_in_s;
					when "01100" => reg_12_o <= mux_data_in_s;
					when "01101" => reg_13_o <= mux_data_in_s;
					when "01110" => reg_14_o <= mux_data_in_s;
					when "01111" => reg_15_o <= mux_data_in_s;
					when "10000" => reg_16_o <= mux_data_in_s;
					when "10001" => reg_17_o <= mux_data_in_s;
					when "10010" => reg_18_o <= mux_data_in_s;
					when "10011" => reg_19_o <= mux_data_in_s;
					when "10100" => reg_20_o <= mux_data_in_s;
					when "10101" => reg_21_o <= mux_data_in_s;
					when "10110" => reg_22_o <= mux_data_in_s;
					when "10111" => reg_23_o <= mux_data_in_s;
					when "11000" => reg_24_o <= mux_data_in_s;
					when "11001" => reg_25_o <= mux_data_in_s;
					when "11010" => reg_26_o <= mux_data_in_s;
					when "11011" => reg_27_o <= mux_data_in_s;
					when "11100" => reg_28_o <= mux_data_in_s;
					when "11101" => reg_29_o <= mux_data_in_s;

					when others => null;
				end case;

			end if;
			
	 	end if;
	end process;


-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- crc process 
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	-- calculate the crc
	crc_process : process (reset_ni, spi_clk_i)
	
	begin
		-- clear registers when reset is low
		if(reset_ni = '0') then

			crc_result_s <= (others => '0');

		-- clock data in on rising edge of spi clock
		elsif rising_edge(spi_clk_i) then	 
		
				-- load address to crc calculator
				crc_result_s <= nextCRC16_D16(crc_data_in_S, crc_calc_s);
		
		
		end if;
	end process;


	
	
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- main mux process 
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	-- receive data from spi module and send to registers
	mux_process : process (reset_ni, spi_clk_i)
	
	begin
		-- clear registers when reset is low
		if(reset_ni = '0') then

			crc_calc_s <= (others => '1');
			
			tx_data_s <= (others => '0');
			register_write_s <= '0';
			register_read_s <= '0';
			freeze_o <= '0';
			error_s <= '0';

		-- clock data in on rising edge of clock
		elsif rising_edge(spi_clk_i) then	 
		

	
			case to_integer(rx_bit_count_s) is
				
				-- first bit, clear flags and crc
				when 1 =>
				
					-- clear crc
					crc_calc_s <= (others => '1');
					
					
					register_write_s <= '0';
					register_read_s <= '0';
					
					tx_data_s <= (others => '0');

				
				-- copy read write bit seperate
				when 7 => tx_data_s(7) <= rx_data_s(0);
				
	
				-- we have the first address byte 
				when 8 =>
				
					-- when '1', freeze stops any modules from updating their output to allow for multiple register reads between updates
					freeze_o <= rx_data_s(1);

				
					-- calculate and check the parity 
					if rx_data_s(7) = (rx_data_s(0) xor rx_data_s(1) xor rx_data_s(2) xor rx_data_s(3) xor rx_data_s(4) xor rx_data_s(5) xor rx_data_s(6)) then

						-- copy first rx bytes to second txt byte
						tx_data_s(8 to 14) <= rx_data_s(1 to 7);
					
					
						-- force a register read, does not matter if its a write cycle as gets filtered on next clock (9)
						register_read_s <= '1';

						error_s <= '0';
					else
						error_s <= '1';

					end if;
					
				when 9 =>

					register_read_s <= '0';
					
				when 10 =>

					-- if rx, load tx data with the register value
					if rx_data_s(0) = '0' and error_s <= '0' then
						tx_data_s(15 to 30) <= mux_data_out_s;
					end if;
					
				-- we have both copies of the address. check if they match
				when 16 =>
				
					-- if both copies match 
					if rx_data_s(0 to 7) = rx_data_s(8 to 15) and error_s = '0' then
					
						-- read
						if rx_data_s(0) = '0' then
					
						
							-- load half address to crc calculator
							crc_data_in_S <= "00000000" & rx_data_s(8 to 15);
							
						else
						
							-- load full address to crc calculator
							crc_data_in_S <= rx_data_s(0 to 15);

						
						end if;

					
					else
					
						error_s <= '1';

					end if;
					
					
					
				when 18 =>
				
					-- if read and no error, load the data to the crc calculator
					if rx_data_s(0) = '0' and error_s = '0' then
						
						crc_data_in_S <= mux_data_out_s;
						crc_calc_s <= crc_result_s;


					end if;
					
					
					
				-- if read and no error, load the tx data with the overall crc	
				when 20 =>
					if rx_data_s(0) = '0' and error_s = '0' then				
						
						tx_data_s(31 to 46) <= crc_result_s;
					
					end if;
				
				-- if a write, then load the crc calculator with the recieved spi data 
				when 32 =>			
					if rx_data_s(0) = '1' and error_s = '0' then	
					
						tx_data_s(31 to 46) <= rx_data_s(16 to 31);
						mux_data_in_s <= rx_data_s(16 to 31);
					
						crc_data_in_S <= rx_data_s(16 to 31);
						crc_calc_s <= crc_result_s;
					
				
					end if;

					
				-- if a write and no error and the received crc matches the calculated crc, 
				-- flag the register write process to do the load
				when 47 =>
				
					if rx_data_s(0) = '1' and error_s = '0' and rx_data_s(32 to 46) & spi_mosi_i = crc_result_s then	
						register_write_s <= '1';
					end if;
					
				when others =>
					null;
				
			end case;
					
				
						
	 	end if;
	end process;

	
	
end architecture RTL;