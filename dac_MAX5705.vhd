-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  NG PVU
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  dac_MAX5705.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  16/11/2018 
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Controls an SPI master to drive the MAX5705 DAC
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.03: Mark Bonning - added code to detect if o/p enable is low and if it is, 
--                            then restart state machine.
-- Issue 0.02: Mark Bonning - changed setup registers as the values in the spec do not work
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
--use IEEE.numeric_std.all;
USE ieee.std_logic_arith.all;


entity dac_max5705 is
    port 	(

     	clock20mhz_i        :  in std_logic;
     	reset_ni            :  in std_logic;
        
     	enable_o				: out std_logic;
     	cpol_o				: out std_logic;
		cpha_o				: out std_logic;
		cont_o				: out std_logic;
		tx_data_o			: out std_logic_vector(23 downto 0);
		busy_i				: in std_logic;
		rx_data_i			: in std_logic_vector(15 downto 0);
		output_enable_i	: in std_logic;
		pulse_1ms_i			: in std_logic


        );
end;

architecture RTL of dac_max5705 is

---- Constants -------------------------------------------------------------------------------

constant number_of_config_bytes_c : integer := 3;

constant power_up_delay_c : integer := 200;
constant write_register_c : std_logic_vector(7 downto 0) := x"a0";

----------------------------------------------------------------------------------------------

---- Signals ---------------------------------------------------------------------------------


type states is ( START, POWER_UP_DELAY, SETUP, SETUP_WAIT, SETUP_LOOP, 
					  CODE_CHANGE, CODE, CODE_WAIT);
signal state   	: states;

signal rx_data_s	: std_logic_vector(15 downto 0);

----------------------------------------------------------------------------------------------


begin

	-- drive constant pins
	cpol_o <= '0';
	cpha_o <= '1';
	cont_o <= '0';

	
----------------------------------------------------------------------------------------------
-- process to control the DAC
----------------------------------------------------------------------------------------------
dac_set : 	process ( reset_ni, clock20mhz_i ) is

	variable count : integer range 0 to 7:= 0;
	
	-- variables for setup lookup table
    type MEM is array(0 to 6) of std_logic_vector(23 downto 0);
    variable dac_setup : MEM;
	
	 variable delay_count : integer range 0 to power_up_delay_c;
    
    
    begin
	 
		 -- register setup values;
	--	 dac_setup := ( x"350000", x"250000", x"400000", x"500030", x"600080", x"700000", x"000000" );
		 dac_setup := ( x"250000", x"250000", x"400000", x"500030", x"600080", x"700000", x"000000" );
	 
        if ( reset_ni = '0' ) then
            enable_o <= '0';
            count := 0;
				delay_count := 0;
				rx_data_s <= (others => '0');
            state <= SETUP;

        elsif Rising_Edge ( clock20mhz_i ) then
        

-- state machine to run the transfer to the SPI module

----------------------------------------------------
-- Send setup data value - loop through each register
----------------------------------------------------

					case state is
					
						-- start state
						when START =>
							rx_data_s <= (others => '0');
							delay_count := 0;
						   count := 0;
							
							-- wait for output enable to allow the dac to start
							if output_enable_i = '1' then
								state <= POWER_UP_DELAY;
							else
								state <= START;
							end if;
					
						-- provide a power up delay to allow supplies to settle
						when POWER_UP_DELAY =>
						
							-- move states if delay count met
							if delay_count = power_up_delay_c then
								state <= SETUP;
							-- increment count every 1ms
							elsif pulse_1ms_i = '1' then
								delay_count := delay_count + 1;
								state <= POWER_UP_DELAY;
							end if;
					
						-- start the setup process
						when SETUP =>
						
							-- start the SPI write
							enable_o <= '1';
							tx_data_o <= dac_setup(count);
							if busy_i = '0' then
								state <= SETUP;
							else
								state <= SETUP_WAIT;
							end if;
						-- wait for SPI completion
						when SETUP_WAIT =>
							enable_o <= '0';
							if busy_i = '0' then
								count := count + 1;
								state <= SETUP_LOOP;
							else
								state <= SETUP_WAIT;
							end if;
							
						-- check if all bytes have been sent
						when SETUP_LOOP =>
							enable_o <= '0';
							if count = number_of_config_bytes_c then
								state <= CODE_CHANGE;
							else
								state <= SETUP;
							end if;


----------------------------------------------------
-- Send CODE value
----------------------------------------------------

						
						-- the dac only gets updated if the data has changed.
						when CODE_CHANGE =>
							
							-- if we have lost o/p enable then return to start  
							if	output_enable_i = '0' then
							
								state <= START;
							
							-- else if value has not changed then wait here
							elsif rx_data_s = rx_data_i  then
							
								state <= CODE_CHANGE;
								
							-- else read new data and start update
							else
								rx_data_s <= rx_data_i;
								state <= CODE;
							end if;
						
						when CODE =>
							enable_o <= '1';
							tx_data_o <= write_register_c & rx_data_s(11 downto 0) & "0000";
							if busy_i = '0' then
								state <= CODE;
							else
								state <= CODE_WAIT;
							end if;
						when CODE_WAIT =>
							enable_o <= '0';
							-- wait while SPI is busy 
							if busy_i = '1'  then
								state <= CODE_WAIT;
							
							else
								state <= CODE_CHANGE;
							end if;

						-- something went wrong, re-setup dac
		        		when others =>
			            count := 0;

							state <= SETUP;
					end case;		          
					
        end if;
    end process;


end architecture RTL;