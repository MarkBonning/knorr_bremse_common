-- -------------------------------------------------------------------------------
--                                                                          
--       Copyright  (c)  2018  ;  Knorr-Bremse Rail Systems (UK) Ltd        
--                                                                            
--        This program is protected by copyright and the information         
--         contained therein is confidential.  The program may not be         
--          copied and the information may not be used or disclosed           
--          except with the written permission of the proprietors            
--                        Knorr-Bremse Rail Systems (UK) Ltd.                 
--                                                                           
-- -------------------------------------------------------------------------------
--                                                                           
--             Project Name  :  PVU Test FPGA
--                FPGA Name  :  Altera 10M08SAU169I7G                          
--                File Name  :  spi_slave.vhd                                  
--                   Author  :  Mark Bonning 
--                     Date  :  17/10/2018 
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Generic spi slave interface. To be used on all FPGAs within the project
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning - based on design by Leesa Kingman. Changed to generic
--             design to connect to a register mux       
-- -------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity spi_slave is
	port 	(
						
		
		
		spi_clk_i      				: in std_logic;  								-- spi clk from master
		spi_cs_ni        				: in std_logic;  								-- active low slave select
		spi_mosi_i         			: in std_logic;  								-- master out, slave in
		spi_miso_o         			: out std_logic;  								-- master in, slave out

		mux_register_o					: out std_logic_vector(5 downto 0); 			-- register address to mux
		mux_full_address_o			: out std_logic_vector(15 downto 0); 			-- full address word
		mux_data_i  					: in std_logic_vector(15 downto 0); 			-- data from mux
		mux_data_o						: out std_logic_vector(15 downto 0);			-- data to mux

		mux_full_address_done_o		: out std_logic;								-- tell the muc the full address is done
		mux_write_o						: out std_logic;								-- write to mux
		mux_read_o						: out std_logic;								-- read from mux
		
		mux_crc_o						: out std_logic_vector(15 downto 0);			-- crc data to the mux
		mux_crc_i						: in std_logic_vector(15 downto 0)				-- crc data from the mux
		);
end; 

architecture RTL of spi_slave is

	signal tx_data_s  				: std_logic_vector(0 to 47);  					-- spi transmit buffer (address copy, data and crc)
	signal error_s						: std_logic;
	signal nread_write_s 			: std_logic;


	signal rx_data_s					: std_logic_vector(0 to 48);					-- spi recieve buffer (including address bytes and crc)


	signal tx_bit_count_s 			: unsigned(7 downto 0);
	signal rx_bit_count_s 			: unsigned(7 downto 0);

	signal spi_mosi_s : std_logic;
	
begin
		
--map registers to ports
	

	spi_mosi_s <= spi_mosi_i;
	
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- SPI TX
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	spi_tx : process (spi_cs_ni, spi_clk_i)
	
	
	begin
		if spi_cs_ni = '1' then

			tx_bit_count_s <= (others => '0');
			
			spi_miso_o <= '0';

		elsif falling_edge (spi_clk_i) then

			spi_miso_o <= tx_data_s(to_integer(tx_bit_count_s));  
			
			if tx_bit_count_s < 47 then
				tx_bit_count_s <= unsigned(tx_bit_count_s) + 1;
			end if;

		end if;
	end process;	


-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- SPI RX
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	spi_rx : process (spi_cs_ni, spi_clk_i)
	
	
	begin
	-- Use the chip select signal to clear all the counts and flags
		if spi_cs_ni = '1' then

			rx_bit_count_s 				<= (others => '0');
			tx_data_s 						<= (others => '0');
			rx_data_s 						<= (others => '0');
			mux_register_o					<= (others => '0');
			mux_full_address_o			<= (others => '0');
			mux_full_address_done_o 	<= '0';
--			mux_write_o 					<= '0';
			mux_read_o 						<= '0';
			nread_write_s					<= '0';
			error_s 							<= '0';
			
--			mux_data_o					<= (others => '0');
--			mux_crc_o					<= (others => '0');
			

		-- clock the process o nthe ruising edge of the SPI clock
		elsif rising_edge (spi_clk_i) then

			-- load the rx register
			rx_data_s(to_integer(rx_bit_count_s)) <= spi_mosi_s;

			-- increment the bit count each lap as long as we dont over run the buffers.
			--if you send an SPI message with extra bits / bytes this will stop it from crashing
			if rx_bit_count_s < 47 then
				rx_bit_count_s <= unsigned(rx_bit_count_s) + 1;
			end if;

			-- copy the first address byte to the second
			if rx_bit_count_s < 8 then
				tx_data_s(to_integer(rx_bit_count_s) + 7) <= spi_mosi_s;
			end if;
			
			
			if rx_bit_count_s = 0 then
				mux_write_o 					<= '0';
			
			
			-- load the register address and calc parity
			elsif  rx_bit_count_s = 7 then

				mux_data_o					<= (others => '0');
				mux_crc_o					<= (others => '0');
			
			
				-- calculate the parity and load tx_buffer with read/write, address and parity if no error load the signals
				if spi_mosi_s = (rx_data_s(0) xor rx_data_s(1) xor rx_data_s(2) xor rx_data_s(3) xor rx_data_s(4) xor rx_data_s(5) xor rx_data_s(6)) then

					-- store the address
					mux_register_o <= rx_data_s(1 to 6);

					-- store read / nwrite 
					nread_write_s <= rx_data_s(0);
					
				end if;
	
			-- if no error, check the second copy of read/write, address and parity matches the first copy
			elsif rx_bit_count_s = 15  then

				-- if both copies match 
				if rx_data_s(0 to 7) = rx_data_s(8 to 14) & spi_mosi_s then

					-- set the out data to be used by the mux for crc checking
					mux_full_address_o <= rx_data_s(0 to 14) & spi_mosi_s;

				end if;

				-- tell the mux the first word (full address) is completly loaded
				mux_full_address_done_o <= '1';

				-- if it is a read transfer, set the read bit
				if nread_write_s = '0' then

					-- tell the mux it a read transfer so prepare the data 
					mux_read_o <= '1';

					-- copy mux data to the tx buffer
					tx_data_s(15 to 30) <= mux_data_i;

				end if;


				-- data transfer complete, crc to go
				elsif rx_bit_count_s = 31 then

					-- if transmit tell the mux to copy back the crc (this is to help with testing)
					if nread_write_s = '1' then
						mux_write_o <= '1';
						mux_data_o <= rx_data_s(16 to 30) & spi_mosi_s;
					end if;

					-- copy the returned data word ready for SPI tx
					tx_data_s(31 to 46) <= mux_crc_i;

				elsif rx_bit_count_s = 32 then
				
					mux_write_o <= '0';
				

				-- flag that the data is present on mux_data_o (until spi cs goes high)
				elsif rx_bit_count_s = 47 then -- and error_s = '0' then

					-- if its a write transfer, set the write bit and copy crc to test the mux to 
					-- compare the transfered crc with the calculted crc
					if nread_write_s = '1' then
						mux_write_o <= '1';
						mux_crc_o <= rx_data_s(32 to 46) & spi_mosi_s;
					end if;

--			else
				-- clear flags
--				mux_write_o <= '0';
--				mux_full_address_done_o <= '0';

			end if;

		end if;
	end process;	

-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-------------------------------------------------------------------------------------------------------------------------------------------------------------------

	
end architecture RTL;