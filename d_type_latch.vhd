-- -------------------------------------------------------------------------------
--                                                                          
--       Copyright  (c)  2018  ;  Knorr-Bremse Rail Systems (UK) Ltd        
--                                                                            
--        This program is protected by copyright and the information         
--         contained therein is confidential.  The program may not be         
--          copied and the information may not be used or disclosed           
--          except with the written permission of the proprietors            
--                        Knorr-Bremse Rail Systems (UK) Ltd.                 
--                                                                           
-- -------------------------------------------------------------------------------
--                                                                           
--             Project Name  :  EX FPGA
--                FPGA Name  :  Altera 10M08SAU169I7G                          
--                File Name  :  d_type_latch.vhd                                  
--                   Author  :  Mark Bonning 
--                     Date  :  14/02/2020
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- D type latch using main clock domain for rising edge
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning - initial design
--             
-- -------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.std_logic_unsigned.all;


entity d_type_latch is
	port 	(
	
		clock20mhz_i	: in std_logic;		
		reset_ni			: in std_logic;		
		
		d_i				: in std_logic;									
		clk_i				: in std_logic;									
		r_i				: in std_logic;									

		q_o				: out std_logic;									
		nq_o				: out std_logic

		
		);
end; 

architecture RTL of d_type_latch is




---- Constants -------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------

---- Signals ---------------------------------------------------------------------------------


-- signals

-- rising edge detect signals
signal s0_s							: std_logic;								-- edge detect signal 0
signal s1_s							: std_logic;								-- edge detect signal 1
signal rising_detect_s			: std_logic;								-- single clock pulse on rising edge



begin

	
-- single clock pulse on rising edge of Tacho_Input_i
rising_detect_s <= not s1_s and s0_s;


----------------------------------------------------------------------------------------------
-- Rising edge logic
----------------------------------------------------------------------------------------------
	rising_detect : process (reset_ni, clock20mhz_i)
	begin
		if reset_ni = '0' then
			s0_s <= '0';
			s1_s <= '0';
		elsif rising_edge (clock20mhz_i) then
			s0_s <= clk_i;
			s1_s <= s0_s;
		end if;
	end process;


----------------------------------------------------------------------------------------------
-- D Type Latch
----------------------------------------------------------------------------------------------

	d_type : process (reset_ni, clock20mhz_i)
	
	begin
		-- clear registers when reset is low
		if(reset_ni = '0') then

		q_o 	<= '0';
		nq_o 	<= '1';
		
		-- clock data in on rising edge of clock
		elsif rising_edge(clock20mhz_i) then	 
			
			if r_i = '1' then
			
				q_o 	<= '0';
				nq_o 	<= '1';
			
			else
			
				if rising_detect_s = '1' then
				
					q_o 	<= d_i;
					nq_o 	<= not d_i;
				
				
				end if;
			
			end if;
			
			
	 	end if;
	end process;
	
end architecture RTL;

 