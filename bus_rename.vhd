-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- TEST MODULE ONLY
-- -------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;


entity bus_rename is
    port (

		reg_i			: in std_logic_vector(11 downto 0);
		reg_o			: out std_logic_vector(15 downto 0)
				  
		  
    );
end entity bus_rename;

architecture RTL of bus_rename is

	

begin

-- logic

reg_o <= "0000" & reg_i;

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

end architecture RTL;

