-- -------------------------------------------------------------------------------
--                                                                          
--       Copyright  (c)  2018  ;  Knorr-Bremse Rail Systems (UK) Ltd        
--                                                                            
--        This program is protected by copyright and the information         
--         contained therein is confidential.  The program may not be         
--          copied and the information may not be used or disclosed           
--          except with the written permission of the proprietors            
--                        Knorr-Bremse Rail Systems (UK) Ltd.                 
--                                                                           
-- -------------------------------------------------------------------------------
--                                                                           
--             Project Name  :  PVU Test FPGA
--                FPGA Name  :  Altera 10M08SAU169I7G                          
--                File Name  :  spi_slave.vhd                                  
--                   Author  :  Mark Bonning 
--                     Date  :  17/10/2018 
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Generic spi slave interface. To be used on all FPGAs within the project
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning - based on design by Leesa Kingman. Changed to generic
--             design to connect to a register mux       
-- -------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity spi_slave is
	port 	(
		spi_clk_i      				: in std_logic;  								-- spi clk from master
		spi_cs_ni        				: in std_logic;  								-- active low slave select
		spi_mosi_i         			: in std_logic;  								-- master out, slave in
		spi_miso_o         			: out std_logic;  								-- master in, slave out

		mux_register_o					: out std_logic_vector(5 downto 0); 			-- register address to mux
		mux_full_address_o					: out std_logic_vector(15 downto 0); 			-- full address word
		mux_data_i  					: in std_logic_vector(15 downto 0); 			-- data from mux
		mux_data_o						: out std_logic_vector(15 downto 0);			-- data to mux

		mux_full_address_done_o					: out std_logic;								-- tell the muc the full address is done
		mux_write_o						: out std_logic;								-- write to mux
		mux_read_o						: out std_logic;								-- read from mux
		
		mux_crc_o					: out std_logic_vector(15 downto 0);			-- crc data to the mux
		mux_crc_i					: in std_logic_vector(15 downto 0)				-- crc data from the mux
		);
end; 

architecture RTL of spi_slave is

	signal rx_data_s		: std_logic_vector(47 downto 0);					-- spi recieve buffer (including address bytes and crc)
	signal tx_data_s  		: std_logic_vector(0 to 47);  					-- spi transmit buffer (address copy, data and crc)
	signal bit_count_s		: unsigned(5 downto 0);
	signal error_s			: std_logic;
	signal nread_write_s :std_logic;

begin
		
	--map registers to ports
	
	

	-- receive data from spi bus
	receive_data : process (spi_cs_ni, spi_clk_i)
	
	begin
		-- clear registers when cs is high
		if(spi_cs_ni = '1') then
			rx_data_s					<= (others => '0');
			mux_register_o				<= (others => '0');
			mux_full_address_o		<= (others => '0');
			bit_count_s					<= (others => '0');
			mux_crc_o					<= (others => '0');
			mux_write_o 				<= '0';
			error_s						<= '0';
			mux_data_o 					<= (others => '0');
			mux_read_o 					<= '0';
			nread_write_s				<= '0';
			mux_full_address_done_o <= '0';
			tx_data_s 					<= (others => '0');

			
		-- clock data in on rising edge of spi clock
		elsif rising_edge(spi_clk_i) then	 

			-- copy mux data to the tx buffer
			tx_data_s(16 to 31) <= mux_data_i;
			
			-- copy the crc to the tx buffer
			tx_data_s(32 to 47) <= mux_crc_i;
		
		
			rx_data_s <= rx_data_s(46 downto 0) & spi_mosi_i;  

			-- load the register address and calc parity
			if  bit_count_s =  7 then
			
				-- calculate the parity and load tx_buffer with read/write, address and parity if no error load the signals
				if spi_mosi_i = (rx_data_s(6) xor rx_data_s(5) xor rx_data_s(4) xor rx_data_s(3) xor rx_data_s(2) xor rx_data_s(1) xor rx_data_s(0)) then

					-- store the address
					mux_register_o <= rx_data_s(5 downto 0);

					-- store read / nwrite 
					nread_write_s <= rx_data_s(6);
				
					-- write the read/write bit, address bits and parity bit to the tx buffer
					tx_data_s(8 to 15) <= rx_data_s(6 downto 0) & spi_mosi_i;

				-- else error
				else
					
					-- set the error
					error_s <= '1';
					
				end if;
				
				
			-- if no error, check the second copy of read/write, address and parity matches the first copy
			elsif bit_count_s = 15 and error_s = '0' then
				
				-- if both copies match and a its a register read
				if rx_data_s(14 downto 7) = rx_data_s(6 downto 0) & spi_mosi_i then

					-- set the out data to be used by the mux for crc checking
					mux_full_address_o <= rx_data_s(14 downto 0) & spi_mosi_i;
					
					-- tell the mux if read or write 
					mux_read_o <= not nread_write_s;
					mux_full_address_done_o <= '1';

				else
				
					-- set the error
					error_s <= '1';
					
				end if;
				
				
				
			-- flag that the data is present on mux_data_o (until spi cs goes high)
			elsif bit_count_s = 47 and error_s = '0' then

				if nread_write_s = '1' then
					mux_write_o <= '1';
					mux_data_o <= rx_data_s(30 downto 15);
					mux_crc_o <= rx_data_s(14 downto 0) & spi_mosi_i;
				end if;
				
			else
				-- clear flags
				mux_read_o 	<= '0';
				mux_write_o <= '0';
				mux_full_address_done_o <= '0';
				
			end if;
			
			-- increment the bit count 
			if bit_count_s < 47 then
				bit_count_s <= bit_count_s + 1;
			end if;
			
	
	 	end if;
	end process;

	
	-- transmit data to spi bus
	transmit_data : process (spi_cs_ni, spi_clk_i, mux_data_i)
	begin
		-- clear registers when cs is high
		if(spi_cs_ni = '1') then

			spi_miso_o <= 'Z';
			
		--clock data out on falling edge of spi clock
		elsif falling_edge(spi_clk_i) then

			spi_miso_o <= tx_data_s(to_integer(bit_count_s));  

		end if;
	end process;
	
	
end architecture RTL;