-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  VD fpga
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  axle_x_timer_enable.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  25/04/2019
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Controls the WSP timer enable section of the Valve Drive FPGA
-- Used in VD fpga and LW fpga
--
-- In LW there is no health timer, so dive the health timer trip 
-- with zero to keep OR funtion happy
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;


entity axle_x_timer_enable is
    port (
	 
		clock20mhz_i             			: in std_logic;
		reset_ni  								: in std_logic;
    
		wsp_axle_x_hold_i							: in std_logic;			

		emergency_i									: in std_logic;			

		axle_x_timer_enable_o					: out std_logic;			-- Internal FPGA signal to Axle x Hold and Vent timer blocks.
		
		axle_x_timer_trip_i						: in std_logic				-- OR'd result of timer trip signal and emergency

				  
    );
end entity axle_x_timer_enable;

architecture imp_axle_x_timer_enable of axle_x_timer_enable is


---- Signals ---------------------------------------------------------------------------------

	signal	axle_x_wsp_active_s				: std_logic;			
	
	signal latch_q_s								: std_logic;

----------------------------------------------------------------------------------------------
	
	
component d_type_latch
	port(clock20mhz_i : IN STD_LOGIC;
		 reset_ni : IN STD_LOGIC;
		 d_i : IN STD_LOGIC;
		 clk_i : IN STD_LOGIC;
		 r_i : IN STD_LOGIC;
		 q_o : OUT STD_LOGIC;
		 nq_o : OUT STD_LOGIC
	);
end component;
	
	
begin


    d_latch : d_type_latch
    port map (
                clock20mhz_i => clock20mhz_i,
                reset_ni => reset_ni, 
					 
                d_i => '1',
					 clk_i => wsp_axle_x_hold_i,
					 r_i => axle_x_timer_trip_i,
					 
					 q_o => latch_q_s
					 
					 
            );



	-- 
	wsp_active : process (reset_ni, clock20mhz_i)
	
	begin
		
		
		if reset_ni = '0' then

			axle_x_wsp_active_s <= '0';
			
			axle_x_timer_enable_o <= '0';

		-- clock data in on rising edge of 20 mhz clock
		elsif rising_edge(clock20mhz_i) then	 

			axle_x_wsp_active_s <= latch_q_s and wsp_axle_x_hold_i;
			
			axle_x_timer_enable_o <= axle_x_wsp_active_s or emergency_i;
		
		end if;
	end process;
	

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

end architecture imp_axle_x_timer_enable;

