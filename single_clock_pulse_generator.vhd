-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  Common FPGA module
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  single_clock_pulse_generator.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  16/10/2019
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Creates a single 20MHz clock pulse at 1khz (1ms) to be used in delay functions
-- Divides 8kHz input to 1kHz, then 
-- Needs 8khz clock from pll
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;


entity single_clock_pulse_generator is
    port (

			clock20mhz_i           :  in std_logic;
			reset_ni               :  in std_logic;

			pulse_1ms_o  				: out std_logic
	
        );
end;

architecture RTL of single_clock_pulse_generator is

	
---- Constants -------------------------------------------------------------------------------

	constant count_20000_limit_c	: unsigned(15 downto 0) := x"4e20";        -- 20000 = 1ms

----------------------------------------------------------------------------------------------

---- Signals ---------------------------------------------------------------------------------

	signal count_20000_s	: unsigned(15 downto 0);        

----------------------------------------------------------------------------------------------

	
begin


-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Process to divide the clock by 20,000
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	process_pulse_1ms : process (reset_ni, clock20mhz_i)

	
	begin
		-- clear registers when reset is low
		if(reset_ni = '0') then

			count_20000_s <= (others => '0');
			pulse_1ms_o <= '0';

		elsif rising_edge(clock20mhz_i) then	 

			-- increment count on 1ms pulse
			if count_20000_s = count_20000_limit_c then

				count_20000_s <= (others => '0');
				pulse_1ms_o <= '1';
				
			else
			
				count_20000_s <= count_20000_s + 1;
				pulse_1ms_o <= '0';
				
			end if;
			
	 	end if;
	end process;


end architecture RTL;