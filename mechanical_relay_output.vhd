-- -------------------------------------------------------------------------------
--                                                                          
--       Copyright  (c)  2018  ;  Knorr-Bremse Rail Systems (UK) Ltd        
--                                                                            
--        This program is protected by copyright and the information         
--         contained therein is confidential.  The program may not be         
--          copied and the information may not be used or disclosed           
--          except with the written permission of the proprietors            
--                        Knorr-Bremse Rail Systems (UK) Ltd.                 
--                                                                           
-- -------------------------------------------------------------------------------
--                                                                           
--             Project Name  :  EX FPGA
--                FPGA Name  :  Altera 10M08SAU169I7G                          
--                File Name  :  mechanical_relay_output.vhd                                  
--                   Author  :  Mark Bonning 
--                     Date  :  07/01/2019 
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Mechanical relay driver
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning - initial design
--             
-- -------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mechanical_relay_output is
	port 	(
	
		clock20m_i					: in std_logic;		
		reset_ni						: in std_logic;		
		
		relay_read_i				: in std_logic;
		relay_drive_i			: in std_logic;
		
		relay_read_o			: out std_logic;
		relay_drive_o			: out std_logic;
		
		pulse_1ms_i					: in std_logic


		
		);
end; 

architecture RTL of mechanical_relay_output is


---- Signals ---------------------------------------------------------------------------------
	signal rising_running_s			: std_logic;
	signal falling_running_s		: std_logic;
	

	-- rising edge detect signals
	signal s0_s							: std_logic;								-- edge detect signal 0
	signal s1_s							: std_logic;								-- edge detect signal 1
	signal rising_detect_s			: std_logic;								-- single clock pulse on rising edge
	signal falling_detect_s			: std_logic;								-- single clock pulse on falling edge

----------------------------------------------------------------------------------------------
	
	
	
begin
		
--map registers to ports
relay_drive_o <= relay_drive_i;
	
-- single clock pulse on rising edge of relay_drive_i
rising_detect_s <= not s1_s and s0_s;
falling_detect_s <= not s0_s and s1_s;
	

	
----------------------------------------------------------------------------------------------
-- Rising / falling edge logic
----------------------------------------------------------------------------------------------
	rising_detect : process (reset_ni, clock20m_i)
	begin
		if reset_ni = '0' then
			s0_s <= '0';
			s1_s <= '0';
		elsif rising_edge (clock20m_i) then
			s0_s <= relay_drive_i;
			s1_s <= s0_s;
		end if;
	end process;	
	

----------------------------------------------------------------------------------------------
-- Relay Read driver
----------------------------------------------------------------------------------------------
	relay_read_process : process (reset_ni, clock20m_i)
	begin
		if reset_ni = '0' then

			relay_read_o <= '0';

		elsif rising_edge (clock20m_i) then


			-- relay_read_o  Output is logic 1 when the 50ms counters are running, 
			if rising_running_s = '1' or falling_running_s = '1' then
			
				relay_read_o <= '1';
				
			
			-- else it follows register value
			else
			
				relay_read_o <= relay_read_i;
				
			end if;

		end if;
	end process;	

	

----------------------------------------------------------------------------------------------
-- Rising edge logic and timer
----------------------------------------------------------------------------------------------


	rising_relay_control : process (reset_ni, clock20m_i)

	variable rising_count	      : integer range 0 to 50 := 0;
	
	begin
		-- clear registers when reset is low
		if(reset_ni = '0') then
			rising_count := 0;
			rising_running_s <= '0';


		-- clock data in on rising edge of clock
		elsif rising_edge(clock20m_i) then	 

			-- if rising edge of relay_in_i, zero the count and set outputs
			if  rising_detect_s = '1' then
				rising_count := 0;
				
				rising_running_s <= '1';
				
			-- if the count gets to 50ms then stop count 
			elsif  rising_count = 50  then

				rising_running_s <= '0';

				
			-- else, increment the count on 1ms pulse
			elsif rising_running_s = '1' and pulse_1ms_i = '1' then

				rising_count := rising_count + 1;
				
			end if;
			
			
			
			
	 	end if;
	end process;
	
----------------------------------------------------------------------------------------------
-- Falling edge logic and timer
----------------------------------------------------------------------------------------------


	falling_relay_control : process (reset_ni, clock20m_i)

	variable falling_count	      : integer range 0 to 50 := 0;
	
	begin
		-- clear registers when reset is low
		if(reset_ni = '0') then
			falling_count := 0;
			falling_running_s <= '0';

		-- clock data in on rising edge of clock
		elsif rising_edge(clock20m_i) then	 

			-- if falling edge of relay_in_i, zero the count and set outputs
			if falling_detect_s = '1' then

				falling_count := 0;
				falling_running_s <= '1';
				
			-- if the count gets to 50ms then stop count 
			elsif falling_count = 50 then

				falling_running_s <= '0';
				
			-- else, increment the count on 1ms pulse
			elsif falling_running_s = '1' and pulse_1ms_i = '1' then

				falling_count := falling_count + 1;
				
			end if;
			
			
	 	end if;
	end process;
	
	
end architecture RTL;