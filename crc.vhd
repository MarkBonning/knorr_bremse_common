-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  PVU
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  crc.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  25/01/2018 
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Clocks 16 bit data words through the CRC function
-- no set number of words, calculates next value on crc_en = 1
-- crc_clear_i clears the crc back to its defult value, ready for the first data
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.crc_package.all;


entity crc is
    port (
        clk_20mhz_i		: in  std_logic;
        reset_ni		: in  std_logic;
        data_in_i			: in  std_logic_vector (15 downto 0);											-- input value to crc calculator (16 bits)
        crc_clear_i		: in  std_logic;																		-- clears the crc ready to start again
        crc_write_i		: in  std_logic;																			-- calculates new crc when = 1
        crc_data_o		: out std_logic_vector (15 downto 0)												-- ouptut crc value
    );
end entity crc;

architecture imp_crc of crc is

   signal crc_out_s: std_logic_vector (15 downto 0);															-- stores the new crc
   signal crc_calc_s: std_logic_vector (15 downto 0);														-- holds the last crc to feed back into the calculator
	signal latch_s : std_logic;																						-- latch to ensure the crc_calc gets loaded at the correct time

begin

	crc_data_o <= crc_out_s;																									-- o/p = current crc value

REGISTERS:
    process (clk_20mhz_i, reset_ni)
    begin
        if reset_ni = '0' then																					-- set values on reset
            crc_calc_s   <= (others => '1');													
            crc_out_s   <= (others => '0');
				latch_s <= '0';
        elsif rising_edge(clk_20mhz_i) then
        	if crc_clear_i = '1' then																				-- clears the crc back to the original value
				crc_calc_s   <= (others => '1');
				crc_out_s   <= (others => '0');
				latch_s <= '0';
			 elsif crc_write_i = '1' then																				-- perform the crc calc
				crc_out_s   <= nextCRC16_D16(data_in_i, crc_calc_s);
				latch_s <= '1';
          elsif latch_s = '1' then																				-- after crc_en goes low:
				crc_calc_s <= crc_out_s;																					-- CRC_EN must go back to 0 before the next data can be clocked through the calculator
				latch_s <= '0';																							-- clear the latch ready for the next data
          end if;
       end if;
    end process;
end architecture imp_crc;