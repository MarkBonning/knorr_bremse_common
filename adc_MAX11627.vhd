-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  EX FPGA
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  adc_MAX11627-MB.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  07/01/2019 
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Controls an SPI master to drive the MAX11627 ADC
-- To keep all MAX11627 modules the same, read the highest number of used channels: 4
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.03: Mark Bonning:	Chnaged timeout code to still run through the data collection 
--								otherwise it can still lock up. We dont copy out the adc read
--								if there was a timeout
--
-- Issue 0.02: Mark Bonning: 	Original code ran 4 spi masters. However, this did not 
--										fit in the fpga, so changed to use 1 SPI master and 
--										manually drive the 4 chip selects. All four simaltaneously 
--										to start a oconversion to sync together.
--										Timeout code added to reset if the end of conversions are
--										not found.
--										Setup data now sent every time to fix any poewr up or power
--										loss issues.
--										
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_unsigned.all;

entity adc_max11627 is
    port 	(

      -- clocks and resets
      clock20mhz_i        :  in std_logic;
      reset_ni            :  in std_logic;
      pulse_1ms_i         :  in std_logic;
        
     	-- SPI config
		enable_o				: out std_logic;
      cpol_o				: out std_logic;
		cpha_o				: out std_logic;
		cont_o				: out std_logic;
		
		-- SPI tx data
		tx_data_o			: out std_logic_vector(7 downto 0);

		-- SPI mater out slave in
		miso_o				: out std_logic;
		
		-- End of conversion inputs
		adc_4_eoc_ni		: in std_logic;
		adc_3_eoc_ni		: in std_logic;
		adc_2_eoc_ni		: in std_logic;
		adc_1_eoc_ni		: in std_logic;

		-- 4 off SPI serial data inputs
		miso_4_i				: in std_logic;
		miso_3_i				: in std_logic;
		miso_2_i				: in std_logic;
		miso_1_i				: in std_logic;

		-- SPI module busy flag
		busy_i				: in std_logic;
		
		-- parallel rx data from SPI
		rx_data_i			: in std_logic_vector(7 downto 0);

		-- chip select from SPI module
		cs_i					: in std_logic;

		-- 4 off adc chips selects
		cs_1_o				: out std_logic;
		cs_2_o				: out std_logic;
		cs_3_o				: out std_logic;
		cs_4_o				: out std_logic;
		
		-- data read from all the adcs
		adc_data_o			: out std_logic_vector(255 downto 0)

		
        );
end;

architecture RTL of adc_max11627 is


---- Constants -------------------------------------------------------------------------------

-- conversion register value to start 4 channels of reads
constant convertion_register_val_c	: std_logic_vector(7 downto 0) := x"98";

-- all chips selects
constant chip_select_all_val_c		: std_logic_vector(3 downto 0) := "1111";

-- just the first adc chip select
constant chip_select_first_val_c		: std_logic_vector(3 downto 0) := "0001";

-- all adc end of conversions 
constant eoc_all_val_c					: std_logic_vector(3 downto 0) := "1111";

-- adc timeout value (1000ms)
constant timeeout_val_c					: integer := 1000;

-- number of setup bytes to write
constant	setup_byte_count_c			: integer := 2;

-- number of data bytes to read (4 adcs x 16 bits)
constant	read_byte_count_c				: integer := 8;

-- number of adcs
constant	number_of_adcs_c				: integer := 4;

----------------------------------------------------------------------------------------------


---- Signals ---------------------------------------------------------------------------------

-- state machine states
type states is ( START, SETUP, SETUP_WAIT, SETUP_LOOP,
					  START_CONVERSION, CONVERSION_WAIT, CONVERSION_DONE, READ_WAIT, READ_READY, READ_DONE );
signal state_s   	: states;

-- register for all data from the adc
signal adc_data_s							: std_logic_vector(255 downto 0);

-- adc chip select
signal cs_s									: std_logic_vector(3 downto 0);

-- adc end of conversion flags (all four adcs)
signal eoc_s								: std_logic_vector(3 downto 0);

-- flag to clear the end of conversion flags
signal clear_eoc_s 						: std_logic;

-- adc timoue. Used if EOC is not recieved within a time frame
signal adc_timeout_s						: std_logic;


----------------------------------------------------------------------------------------------




begin

	-- drive constant pins
	cpol_o <= '1';
	cpha_o <= '1';
	cont_o <= '0';
	
	
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Process to setup and read the adc
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
adc :	process ( reset_ni, clock20mhz_i ) is

		variable byte_count 		: integer range 0 to 10 	:= 0;
		variable adc_count 		: integer range 0 to 4 		:= 0;
		variable timeout_count 	: integer range 0 to 1000 	:= 0;
		
		
		-- variables for setup lookup table
		 type MEM is array(0 to 1) of std_logic_vector(7 downto 0);
		 variable adc_setup : MEM;
    
    
    begin
	 
		 -- register setup values;
		 adc_setup := ( x"68", x"20" );
	 
	 
			-- reset condition
        if ( reset_ni = '0' ) then
				enable_o <= '0';
            byte_count := 0;
				adc_count := 0;
				adc_data_s <= (others => '0');
				adc_data_o <= (others => '0');
				cs_s <= (others => '0');
				clear_eoc_s <= '0';
				eoc_s <= (others => '0');
				adc_timeout_s <= '0';
				timeout_count := 0;
            state_s <= START;

        	elsif Rising_Edge ( clock20mhz_i ) then
		  
		  
				-- chip selects
				cs_1_o <= cs_i or not cs_s(0);
				cs_2_o <= cs_i or not cs_s(1);
				cs_3_o <= cs_i or not cs_s(2);
				cs_4_o <= cs_i or not cs_s(3);
	
	
				-- selects the correct adc for miso data
				miso_o <=  	(miso_1_i and cs_s(0)) or 
								(miso_2_i and cs_s(1)) or
								(miso_3_i and cs_s(2)) or
								(miso_4_i and cs_s(3));
		  
        
			
				-- clear or load end of conversion flags
				if clear_eoc_s = '1' then
					eoc_s <= (others => '0');
				else
					if adc_1_eoc_ni = '0' then
						eoc_s(0) <= '1';
					end if;
					if adc_2_eoc_ni = '0' then
						eoc_s(1) <= '1';
					end if;
					if adc_3_eoc_ni = '0' then
						eoc_s(2) <= '1';
					end if;
					if adc_4_eoc_ni = '0' then
						eoc_s(3) <= '1';
					end if;
				end if;
		  
		  
				-- create 1 second timeout in case adc's lock up or lose power
				-- force a restart in FSM
				if timeout_count = timeeout_val_c then
				
					adc_timeout_s <= '1';
					
				-- increment count on 1ms pulse
				elsif pulse_1ms_i = '1' then
				
					adc_timeout_s <= '0';
					timeout_count := timeout_count + 1;
					
				end if;
		  
		  
-- state machine to run the transfer to the SPI module

----------------------------------------------------
-- Send setup data value - loop through each register
----------------------------------------------------

					-- state machine
					case state_s is
						when START =>
						
							-- clear timeout count
							timeout_count := 0;
							
							-- start on a 1ms pulse and not busy
							if busy_i = '0' and pulse_1ms_i = '1' then
								state_s <= SETUP;
							else
								state_s <= START;
							end if;
							
						-- the next state sends data to all 4 adcs setup registers at the same time
						when SETUP =>

							-- enable the SPI interface
							enable_o <= '1';
							
							-- write to all 4 adcs at the same time
							cs_s <= chip_select_all_val_c;
							
							-- setup data
							tx_data_o <= adc_setup(byte_count);
							
							-- wait for busy to go high to move to the next state
							if busy_i = '0' then
								state_s <= SETUP;
							else
								state_s <= SETUP_WAIT;
							end if;
							
						when SETUP_WAIT =>
						
							enable_o <= '0';
							
							--wait for busy to go low to move to the next state
							if busy_i = '0' then
								-- increment the byte count
								byte_count := byte_count + 1;
								state_s <= SETUP_LOOP;
							else
								state_s <= SETUP_WAIT;
							end if;
							
						-- check if all data sent
						when SETUP_LOOP =>
							enable_o <= '0';
							
							-- if all setup data sent move to the conversion section, else loop again with the next byte
							if byte_count = setup_byte_count_c then
								byte_count := 0;
								state_s <= START_CONVERSION;
							else
								state_s <= SETUP;
							end if;


----------------------------------------------------
-- Perform conversion
----------------------------------------------------

						when START_CONVERSION =>

							clear_eoc_s <= '0';
							adc_count := 0;
						
							-- write to all 4 adcs at the same time
							cs_s <= chip_select_all_val_c;
							enable_o <= '1';
							
							-- scan adc channels 0 through to 3
							tx_data_o <= convertion_register_val_c;	
							
							-- wait for busy to go high to move ot the next state
							if busy_i = '0' then
								state_s <= START_CONVERSION;
							else
								state_s <= CONVERSION_WAIT;
							end if;
						
						-- wait here for all 4 adcs to finish their conversion
						when CONVERSION_WAIT =>
							enable_o <= '0';
							
							-- if all four adc finished:
							if eoc_s = eoc_all_val_c or adc_timeout_s = '1' then

								-- set flag to clear all the storede eoc states
								clear_eoc_s <= '1';

								-- select first adc
								cs_s <= chip_select_first_val_c;
								
								-- move to next state
								state_s <= CONVERSION_DONE;
								
							else
								-- else, wait in this state
								state_s <= CONVERSION_WAIT;
								
							end if;
							
						-- conversion complete, so start to read the data frm the adc's, one at a time
						when CONVERSION_DONE =>
						
							-- enable the SPI interface
							enable_o <= '1';
	
							-- spi data can all be zeros while data is clocked out
							tx_data_o <= (others => '0');
							
							-- wait for busy to go high to move ot the next state
							if busy_i = '0' then
								state_s <= CONVERSION_DONE;
							else
								state_s <= READ_WAIT;
							end if;						

						-- wait for SPI to finish clocking data
						when READ_WAIT =>
							enable_o <= '0';
							
							-- wait for busy to go low to move ot the next state
							if busy_i = '1' then
								state_s <= READ_WAIT;
							else
								-- increment the byte count (rx bytes)
								byte_count := byte_count + 1;
								state_s <= READ_READY;
							end if;						
							
						-- read the data intot the output register
						when READ_READY =>

							-- shift data into the buffer
							adc_data_s <= adc_data_s(247 downto 0) & rx_data_i;
					
							-- if all data received, move ot the next state
							-- eight bytes (4 adcs x 16 bits) (adc data is the 12 least significant bits)
							if byte_count = read_byte_count_c then
							
								-- clear byte count ready for next data set
								byte_count := 0;
								state_s <= READ_DONE;
								
							-- else, get the next byte
							else
								state_s <= CONVERSION_DONE;
							end if;

						-- all bytes for a single adc read, so check if all adc are done
						when READ_DONE =>
								
							-- increment number of adcs read
							adc_count := adc_count + 1;
						
							-- shit to next chip select
							cs_s <= cs_s(2 downto 0) & '0';
							
							-- check if all adcs read
							if adc_count = number_of_adcs_c then
								-- all done, if there was not a timeout, copy data from input buffer to output
								if adc_timeout_s = '0' then
									adc_data_o <= adc_data_s;
								end if;
								state_s <= START;
							-- if not, the read next adc
							else
								state_s <= CONVERSION_DONE;
							end if;
							
						-- something went wrong, re-setup adc
		        		when others =>
							state_s <= START;
					end case;		          
					
        end if;
    end process;


end architecture RTL;